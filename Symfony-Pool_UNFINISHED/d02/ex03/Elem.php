<?php

Class Elem
{
    private $supp = array('meta', 'img', 'hr', 'br', 'html', 'head', 'body', 'title', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'div');
    private $element;
    private $content;
    private $sub;

    public function __construct($element, $content = NULL)
    {
        if (in_array($element, $this->supp) === FALSE)
            exit();
        $this->element = $element;
        $this->content = $content;
        $this->sub = array();
    }

    public function __destruct()
    {
    }

    public function pushElement($elem)
    {
        $this->sub[] = $elem;
    }

    public function getHTML()
    {
        $str = '<'.$this->element.'>';
        if ($this->content !== NULL)
            $str .= $this->content;
        else
            $str .= PHP_EOL;
        foreach($this->sub as $k)
            $str .= $k->getHTML();
        $str .= '</'.$this->element.'>'.PHP_EOL;
        return ($str);
    }

}

?>