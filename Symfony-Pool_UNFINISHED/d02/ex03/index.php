<?php

    require_once('./TemplateEngine.php');

    $elem = new Elem('html');
    $body = new Elem('body');
    $body->pushElement(new Elem('p', 'Lorem ipsum'));
    $elem->pushElement($body);

    $template = new TemplateEngine($elem);
    $template->createFile('index.html');

?>