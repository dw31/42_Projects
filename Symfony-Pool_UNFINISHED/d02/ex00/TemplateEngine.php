<?php

Class TemplateEngine
{
  public function __construct()
  {
  }

  public function __destruct()
  {
  }

  public function createFile($fileName, $templateName, $parameters)
  {
    $str = file_get_contents($templateName);
    foreach ($parameters as $k => $v)
      $str = str_replace('{'.$k.'}', $v, $str);
    file_put_contents($fileName , $str);
  }
}

?>
