<?php

    require_once('./TemplateEngine.php');

    $elem = new Elem('html');
    $body = new Elem('body');
    $body->pushElement(new Elem('p', 'Lorem ipsum', ['class' => 'text-muted']));
    $elem->pushElement($body);
    echo $elem->getHTML();
    $elem = new Elem('undefined');  // Leve une exception de type MyException$

?>