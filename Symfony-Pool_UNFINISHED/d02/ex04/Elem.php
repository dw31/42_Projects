<?php

class MyException extends Exception
{
    public function __construct()
    {
        parent::__construct('Invalid tag', 0, NULL);
    }
}

Class Elem
{
    private $supp = array('meta', 'img', 'hr', 'br', 'html', 'head', 'body', 'title', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'div', 'table', 'tr', 'th', 'td', 'ul', 'ol', 'li');
    private $element;
    private $content;
    private $attributes;
    private $sub;

    public function __construct($element, $content = NULL, $attributes = array())
    {
        if (in_array($element, $this->supp) === FALSE)
            throw new MyException();
        $this->element = $element;
        $this->content = $content;
        $this->attributes = $attributes;
        $this->sub = array();
    }

    public function __destruct()
    {
    }

    public function pushElement($elem)
    {
        $this->sub[] = $elem;
    }

    public function getHTML()
    {
        $str = '<'.$this->element;
        foreach($this->attributes as $k => $v)
            $str .= ' '.$k.'="'.$v.'"';
        $str .= '>';
        if ($this->content !== NULL)
            $str .= $this->content;
        else
            $str .= PHP_EOL;
        foreach($this->sub as $k)
            $str .= $k->getHTML();
        $str .= '</'.$this->element.'>'.PHP_EOL;
        return ($str);
    }

}

?>