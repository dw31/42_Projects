<?php

Class HotBeverage
{
    protected $name;
    protected $price;
    protected $resistence;

    public function __construct()
    {
    }
    public function __destruct()
    {
    }

    public function getName() { return $this->name; }
    public function getPrice() { return $this->price; }
    public function getResistence() { return $this->resistence; }
}

?>
