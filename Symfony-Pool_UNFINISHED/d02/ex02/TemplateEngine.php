<?php

require_once('./HotBeverage.php');

Class TemplateEngine
{
  public function __construct()
  {
  }

  public function __destruct()
  {
  }

  public function createFile(HotBeverage $text)
  {
    $str = file_get_contents('./template.html');
    $RC = new ReflectionClass($text);
    $str = str_replace('{nom}', $text->getName(), $str);
    $str = str_replace('{prix}', $text->getPrice(), $str);
    $str = str_replace('{resistance}', $text->getResistence(), $str);
    $str = str_replace('{description}', $text->getDescription(), $str);
    $str = str_replace('{commentaire}', $text->getComment(), $str);
    file_put_contents($RC->getShortName().'.html' , $str);
  }
}

?>
