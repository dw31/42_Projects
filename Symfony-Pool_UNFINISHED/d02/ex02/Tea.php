<?php

require_once('./HotBeverage.php');

Class Tea extends HotBeverage
{
    private $description;
    private $comment;

    public function __construct()
    {
        $this->name = 'Tea';
        $this->price = 1;
        $this->resistence = 21;
        $this->description = 'English Tea';
        $this->comment = 'Good for pee';
    }
    public function __destruct()
    {
    }
    
    public function getDescription() { return $this->description; }
    public function getComment() { return $this->comment; }
}

?>
