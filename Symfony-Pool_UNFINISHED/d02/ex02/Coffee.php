<?php

require_once('./HotBeverage.php');

Class Coffee extends HotBeverage
{
    private $description;
    private $comment;
    
    public function __construct()
    {
        $this->name = 'Coffee';
        $this->price = 2;
        $this->resistence = 42;
        $this->description = 'Kenyan Coffee';
        $this->comment = 'Good for wake up';
    }
    public function __destruct()
    {
    }

    public function getDescription() { return $this->description; }
    public function getComment() { return $this->comment; }
}

?>
