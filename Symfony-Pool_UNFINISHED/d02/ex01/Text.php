<?php

Class Text
{

  private $_arr;

  public function __construct($arr)
  {
    $this->_arr = $arr;
  }

  public function __destruct()
  {
  }

  public function add($str)
  {
    $this->_arr[] = $str;
  }

  public function display()
  {
    foreach($this->_arr as $k)
      echo '<p>'.$k.'</p>';
  }

}

?>
