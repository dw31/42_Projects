<?php

require('./Text.php');

Class TemplateEngine
{
  public function __construct()
  {
  }

  public function __destruct()
  {
  }

  public function createFile($fileName, Text $text)
  {
    ob_start();

    $text->display();

    $str =
'<!DOCTYPE html>
<html>
  <head>
    <title></title>
  </head>
  <body>
  '.ob_get_contents().'
  </body>
</html>
';

    ob_end_clean();

    file_put_contents($fileName , $str);
  }
}

?>
