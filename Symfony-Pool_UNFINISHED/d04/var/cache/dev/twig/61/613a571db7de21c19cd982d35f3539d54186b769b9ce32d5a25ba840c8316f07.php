<?php

/* @E01/Default/cat_templating.html.twig */
class __TwigTemplate_25fa973af530198cad27ee5bfb33b8b70201e8a15091d82245338d00bb8bb0e1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@E01/base.html.twig", "@E01/Default/cat_templating.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@E01/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@E01/Default/cat_templating.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@E01/Default/cat_templating.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "


    <div class=\"row\">

        <div class=\"span6\">
            <h3>Including partials and components</h3>

";
        // line 18
        echo "
<pre><code>{% include \"Bundle:Controller:action\" %}
<strong>in Symfony 2.1:</strong>
{% render \"Bundle:Controller:action\" with {\"max\" : 3} %}
<strong>in Symfony 2.2:</strong>
{{ render(controller(\"Bundle:Controller:action\", {max :3})) }}
</code></pre>";
        echo "

            <h3>Links</h3>

";
        // line 26
        echo "
<pre><code>&lt;a href=\"{{ path('homepage') }}\"&gt;Home&lt;a/&gt; //relative
&lt;a href=\"{{ url('homepage') }}\"&gt;Home&lt;a/&gt; //absolute
&lt;a href=\"{{ path('show', {'id':article.id}) }}\"&gt;Home&lt;/a&gt;
</code></pre>";
        echo "

            <h3>Assets</h3>

";
        // line 32
        echo "
<pre><code>&lt;img src=\"{{ 'uploads/'~foto.url }}\"/&gt;
</code></pre>";
        echo "

        </div>

        <div class=\"span6\">

            <h3>Debug variables in a template</h3>

            ";
        // line 42
        echo "
            <pre><code>{{ dump(article) }}
            </code></pre>";
        echo "

            <h3>Global TWIG variables</h3>

";
        // line 55
        echo "
<pre><code>app.security
app.user
app.request
app.request.get('foo') //get
app.request.request.get('foo') //post
app.session
app.environment
app.debug
</code></pre>";
        echo "
        </div>

    </div>

    <h3>TWIG TAGS</h3>

    <div class=\"row\">

        <div class=\"span6\">
            <h4>block</h4>

            <p>When a template uses inheritance and if you want to print a block multiple times, use the block function:</p>

";
        // line 73
        echo "
<pre><code>&lt;title&gt;{% block title %}{% endblock %}&lt;/title&gt;
&lt;h1&gt;{{ block('title') }}&lt;/h1&gt;
{% block body %}{% endblock %}
</code></pre>";
        echo "

            <h4>parent</h4>

            <p>When a template uses inheritance, it's possible to render the contents of the parent block when overriding a block by using the parent function:</p>

";
        // line 87
        echo "
<pre><code>{% extends \"base.html\" %}

{% block sidebar %}
&lt;h3&gt;Table Of Contents&lt;/h3&gt;
...
{{ parent() }}
{% endblock %}
</code></pre>";
        echo "

            <h4>for</h4>

            <p>Loop over each item in a sequence. For example, to display a list of users provided in a variable called users:</p>

";
        // line 100
        echo "
<pre><code>&lt;h1&gt;Members&lt;/h1&gt;
&lt;ul&gt;
{% for user in users %}
&lt;li&gt;{{ user.username|e }}&lt;/li&gt;
{% endfor %}
&lt;/ul&gt;
</code></pre>";
        echo "

            <h6>The loop variable</h6>

            <p>Inside of a for loop block you can access some special variables:</p>

";
        // line 116
        echo "
<pre><code>Variable Description
loop.index The current iteration of the loop. (1 indexed)
loop.index0 The current iteration of the loop. (0 indexed)
loop.revindex The number of iterations from the end of the loop (1 indexed)
loop.revindex0 The number of iterations from the end of the loop (0 indexed)
loop.first True if first iteration
loop.last True if last iteration
loop.length The number of items in the sequence
loop.parent The parent context
</code></pre>";
        echo "

";
        // line 122
        echo "
<pre><code>{% for user in users %}
{{ loop.index }} - {{ user.username }}
{% endfor %}
</code></pre>";
        echo "

            <h4>if</h4>

            <p>The if statement in Twig is comparable with the if statements of PHP.</p>

            <p>In the simplest form you can use it to test if an expression evaluates to true:</p>

";
        // line 134
        echo "
<pre><code>{% if online == false %}
&lt;p&gt;Our website is in maintenance mode. Please, come back later.&lt;/p&gt;
{% endif %}
</code></pre>";
        echo "

            <h4>raw</h4>

            <p>Everything inside raw tags won't be parsed.</p>

";
        // line 143
        echo "
<pre><code>{% raw %}
This variable {{foo}} won't be parsed as twig var.
{% endraw. %}</code></pre>";
        echo "

        </div>

        <div class=\"span6\">
            <h4>set</h4>

            <p>Inside code blocks you can also assign values to variables. Assignments use the set tag and can have multiple targets:</p>

";
        // line 158
        echo "
<pre><code>{% set foo = 'foo' %}
{% set foo = [1, 2] %}
{% set foo = {'foo': 'bar'} %}
{% set foo = 'foo' ~ 'bar' %}
{% set foo, bar = 'foo', 'bar' %}
</code></pre>";
        echo "

            <h4>filter</h4>

            <p>Filter sections allow you to apply regular Twig filters on a block of template data. Just wrap the code in the special filter section:</p>

";
        // line 168
        echo "
<pre><code>{% filter upper %}
This text becomes uppercase
{% endfilter %}
</code></pre>";
        echo "

            <p>You can also chain filters:</p>

";
        // line 176
        echo "
<pre><code>{% filter lower|escape %}
&lt;strong&gt;SOME TEXT&lt;/strong&gt;
{% endfilter %}
</code></pre>";
        echo "

            <h4>macro</h4>

            <p>Macros are comparable with functions in regular programming languages. They are useful to put often used HTML idioms into reusable elements to not repeat yourself.</p>

            <p>Here is a small example of a macro that renders a form element:</p>

";
        // line 188
        echo "
<pre><code>{% macro input(name, value, type, size) %}
&lt;input type=\"{{ type|default('text') }}\" name=\"{{ name }}\" value=\"{{ value|e }}\" size=\"{{ size|default(20) }}\" /&gt;
{% endmacro %}
</code></pre>";
        echo "

            <p>Macros differs from native PHP functions in a few ways:</p>

            <ul>
                <li>Default argument values are defined by using the default filter in the macro body;</li>
                <li>Arguments of a macro are always optional.</li>
            </ul>
            <p>But as PHP functions, macros don't have access to the current template variables.</p>

            <p>Macros can be defined in any template, and need to be \"imported\" before being used (see the documentation for the import tag for more information):</p>

";
        // line 202
        echo "
<pre><code>{% import \"forms.html\" as forms %}
</code></pre>";
        echo "

            <p>The above import call imports the \"forms.html\" file (which can contain only macros, or a template and some macros), and import the functions as items of the forms variable.</p>

            <p>The macro can then be called at will:</p>

";
        // line 211
        echo "
<pre><code>&lt;p&gt;{{ forms.input('username') }}&lt;/p&gt;
&lt;p&gt;{{ forms.input('password', null, 'password') }}&lt;/p&gt;
</code></pre>";
        echo "

            <p>If macros are defined and used in the same template, you can use the special _self variable to import them:</p>

";
        // line 218
        echo "
<pre><code>{% import _self as forms %}
&lt;p&gt;{{ forms.input('username') }}&lt;/p&gt;
</code></pre>";
        echo "

        </div>

    </div>

    <h3>TWIG FILTERS</h3>

    <div class=\"row\">

        <div class=\"span6\">

            <h4>date</h4>

            ";
        // line 235
        echo "
<pre><code>{{ post.published_at|date(\"m/d/Y\") }}
{{ post.published_at|date(\"m/d/Y\", \"Europe/Paris\") }}
</code></pre>";
        echo "

            <h4>date_modify</h4>

";
        // line 241
        echo "
<pre><code>{{ post.published_at|date_modify(\"+1 day\")|date(\"m/d/Y\") }}
</code></pre>";
        echo "

            <h4>format</h4>

";
        // line 247
        echo "
<pre><code>{{ \"I like %s and %s.\"|format(foo, \"bar\") }}
</code></pre>";
        echo "

            <h4>replace</h4>

";
        // line 253
        echo "
<pre><code>{{ \"I like %this% and %that%.\"|replace({'%this%': foo, '%that%': \"bar\"}
</code></pre>";
        echo "

            <h4>number_format</h4>

";
        // line 260
        echo "
<pre><code>{{ 200.35|number_format }}
{{ 9800.333|number_format(2, '.', ',') }}
</code></pre>";
        echo "

            <h4>url_encode</h4>

";
        // line 266
        echo "
<pre><code>{{ data|url_encode() }}
</code></pre>";
        echo "

            <h4>json_encode</h4>

";
        // line 272
        echo "
<pre><code>{{ data|json_encode() }}
</code></pre>";
        echo "

            <h4>convert_encoding</h4>

";
        // line 278
        echo "
<pre><code>{{ data|convert_encoding('UTF-8', 'iso-2022-jp') }}
</code></pre>";
        echo "

            <h4>title</h4>

";
        // line 285
        echo "
<pre><code>{{ 'my first car'|title }}
{# outputs 'My First Car' #}
</code></pre>";
        echo "

            <h4>capitalize</h4>

";
        // line 291
        echo "
<pre><code>{{ 'my first car'|capitalize }}
</code></pre>";
        echo "

            <h4>nl2br</h4>

";
        // line 303
        echo "
<pre><code>{{ \"I like Twig.\\nYou will like it too.\"|nl2br }}
    {# outputs

    I like Twig.&lt;br /&gt;
    You will like it too.

    #}
</code></pre>";
        echo "

            <h4>raw</h4>

";
        // line 309
        echo "
<pre><code>{{ var|raw }} {# var won't be escaped #}
</code></pre>";
        echo "

            <h4>trim</h4>

";
        // line 315
        echo "
<pre><code>{{ ' I like Twig.'|trim('.') }}
</code></pre>";
        echo "
        </div>

        <div class=\"span6\">
            <h4>upper</h4>

";
        // line 323
        echo "
<pre><code>{{ 'welcome'|upper }}
</code></pre>";
        echo "

            <h4>lower</h4>

";
        // line 329
        echo "
<pre><code>{{ 'WELCOME'|lower }}
</code></pre>";
        echo "

            <h4>striptags</h4>

";
        // line 335
        echo "
<pre><code>{% some_html|striptags %}
</code></pre>";
        echo "

            <h4>join</h4>

";
        // line 342
        echo "
<pre><code>{{ [1, 2, 3]|join('|') }}
{# returns 1|2|3 #}
</code></pre>";
        echo "

            <h4>split</h4>

";
        // line 349
        echo "
<pre><code>{{ \"one,two,three\"|split(',') }}
{# returns ['one', 'two', 'three'] #}
</code></pre>";
        echo "

            <h4>reverse</h4>

";
        // line 357
        echo "
<pre><code>{% for use in users|reverse %}
    ...
{% endfor %}
</code></pre>";
        echo "

            <h4>abs</h4>

";
        // line 363
        echo "
<pre><code>{{ number|abs }}
</code></pre>";
        echo "

            <h4>length</h4>

";
        // line 371
        echo "
<pre><code>{% if users|length &gt; 10 %}
    ...
{% endif %}
</code></pre>";
        echo "

            <h4>sort</h4>

";
        // line 379
        echo "
<pre><code>{% for use in users|sort %}
    ...
{% endfor %}
</code></pre>";
        echo "

            <h4>default</h4>

";
        // line 385
        echo "
<pre><code>{{ var|default('var is not defined') }}
</code></pre>";
        echo "

            <h4>keys</h4>

";
        // line 393
        echo "
<pre><code>{% for key in array|keys %}
    ...
{% endfor %}
</code></pre>";
        echo "

            <h4>escape</h4>

";
        // line 403
        echo "
<pre><code>{{ user.username|e }}
{# is equivalent to #}
{{ user.username|e('html') }}
{{ user.username|e('css') }}
{{ user.username|e('js') }}
</code></pre>";
        echo "

        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@E01/Default/cat_templating.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  573 => 403,  562 => 393,  553 => 385,  542 => 379,  531 => 371,  522 => 363,  511 => 357,  501 => 349,  491 => 342,  482 => 335,  473 => 329,  464 => 323,  453 => 315,  444 => 309,  429 => 303,  420 => 291,  410 => 285,  401 => 278,  392 => 272,  383 => 266,  373 => 260,  364 => 253,  355 => 247,  346 => 241,  336 => 235,  316 => 218,  306 => 211,  295 => 202,  276 => 188,  261 => 176,  250 => 168,  235 => 158,  220 => 143,  207 => 134,  192 => 122,  177 => 116,  161 => 100,  144 => 87,  131 => 73,  105 => 55,  96 => 42,  83 => 32,  72 => 26,  59 => 18,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@E01/base.html.twig' %}

{% block content %}



    <div class=\"row\">

        <div class=\"span6\">
            <h3>Including partials and components</h3>

{% raw %}
<pre><code>{% include \"Bundle:Controller:action\" %}
<strong>in Symfony 2.1:</strong>
{% render \"Bundle:Controller:action\" with {\"max\" : 3} %}
<strong>in Symfony 2.2:</strong>
{{ render(controller(\"Bundle:Controller:action\", {max :3})) }}
</code></pre>{% endraw %}

            <h3>Links</h3>

{% raw %}
<pre><code>&lt;a href=\"{{ path('homepage') }}\"&gt;Home&lt;a/&gt; //relative
&lt;a href=\"{{ url('homepage') }}\"&gt;Home&lt;a/&gt; //absolute
&lt;a href=\"{{ path('show', {'id':article.id}) }}\"&gt;Home&lt;/a&gt;
</code></pre>{% endraw %}

            <h3>Assets</h3>

{% raw %}
<pre><code>&lt;img src=\"{{ 'uploads/'~foto.url }}\"/&gt;
</code></pre>{% endraw %}

        </div>

        <div class=\"span6\">

            <h3>Debug variables in a template</h3>

            {% raw %}
            <pre><code>{{ dump(article) }}
            </code></pre>{% endraw %}

            <h3>Global TWIG variables</h3>

{% raw %}
<pre><code>app.security
app.user
app.request
app.request.get('foo') //get
app.request.request.get('foo') //post
app.session
app.environment
app.debug
</code></pre>{% endraw %}
        </div>

    </div>

    <h3>TWIG TAGS</h3>

    <div class=\"row\">

        <div class=\"span6\">
            <h4>block</h4>

            <p>When a template uses inheritance and if you want to print a block multiple times, use the block function:</p>

{% raw %}
<pre><code>&lt;title&gt;{% block title %}{% endblock %}&lt;/title&gt;
&lt;h1&gt;{{ block('title') }}&lt;/h1&gt;
{% block body %}{% endblock %}
</code></pre>{% endraw %}

            <h4>parent</h4>

            <p>When a template uses inheritance, it's possible to render the contents of the parent block when overriding a block by using the parent function:</p>

{% raw %}
<pre><code>{% extends \"base.html\" %}

{% block sidebar %}
&lt;h3&gt;Table Of Contents&lt;/h3&gt;
...
{{ parent() }}
{% endblock %}
</code></pre>{% endraw %}

            <h4>for</h4>

            <p>Loop over each item in a sequence. For example, to display a list of users provided in a variable called users:</p>

{% raw %}
<pre><code>&lt;h1&gt;Members&lt;/h1&gt;
&lt;ul&gt;
{% for user in users %}
&lt;li&gt;{{ user.username|e }}&lt;/li&gt;
{% endfor %}
&lt;/ul&gt;
</code></pre>{% endraw %}

            <h6>The loop variable</h6>

            <p>Inside of a for loop block you can access some special variables:</p>

{% raw %}
<pre><code>Variable Description
loop.index The current iteration of the loop. (1 indexed)
loop.index0 The current iteration of the loop. (0 indexed)
loop.revindex The number of iterations from the end of the loop (1 indexed)
loop.revindex0 The number of iterations from the end of the loop (0 indexed)
loop.first True if first iteration
loop.last True if last iteration
loop.length The number of items in the sequence
loop.parent The parent context
</code></pre>{% endraw %}

{% raw %}
<pre><code>{% for user in users %}
{{ loop.index }} - {{ user.username }}
{% endfor %}
</code></pre>{% endraw %}

            <h4>if</h4>

            <p>The if statement in Twig is comparable with the if statements of PHP.</p>

            <p>In the simplest form you can use it to test if an expression evaluates to true:</p>

{% raw %}
<pre><code>{% if online == false %}
&lt;p&gt;Our website is in maintenance mode. Please, come back later.&lt;/p&gt;
{% endif %}
</code></pre>{% endraw %}

            <h4>raw</h4>

            <p>Everything inside raw tags won't be parsed.</p>

{% raw %}
<pre><code>{% raw %}
This variable {{foo}} won't be parsed as twig var.
{% endraw. %}</code></pre>{% endraw %}

        </div>

        <div class=\"span6\">
            <h4>set</h4>

            <p>Inside code blocks you can also assign values to variables. Assignments use the set tag and can have multiple targets:</p>

{% raw %}
<pre><code>{% set foo = 'foo' %}
{% set foo = [1, 2] %}
{% set foo = {'foo': 'bar'} %}
{% set foo = 'foo' ~ 'bar' %}
{% set foo, bar = 'foo', 'bar' %}
</code></pre>{% endraw %}

            <h4>filter</h4>

            <p>Filter sections allow you to apply regular Twig filters on a block of template data. Just wrap the code in the special filter section:</p>

{% raw %}
<pre><code>{% filter upper %}
This text becomes uppercase
{% endfilter %}
</code></pre>{% endraw %}

            <p>You can also chain filters:</p>

{% raw %}
<pre><code>{% filter lower|escape %}
&lt;strong&gt;SOME TEXT&lt;/strong&gt;
{% endfilter %}
</code></pre>{% endraw %}

            <h4>macro</h4>

            <p>Macros are comparable with functions in regular programming languages. They are useful to put often used HTML idioms into reusable elements to not repeat yourself.</p>

            <p>Here is a small example of a macro that renders a form element:</p>

{% raw %}
<pre><code>{% macro input(name, value, type, size) %}
&lt;input type=\"{{ type|default('text') }}\" name=\"{{ name }}\" value=\"{{ value|e }}\" size=\"{{ size|default(20) }}\" /&gt;
{% endmacro %}
</code></pre>{% endraw %}

            <p>Macros differs from native PHP functions in a few ways:</p>

            <ul>
                <li>Default argument values are defined by using the default filter in the macro body;</li>
                <li>Arguments of a macro are always optional.</li>
            </ul>
            <p>But as PHP functions, macros don't have access to the current template variables.</p>

            <p>Macros can be defined in any template, and need to be \"imported\" before being used (see the documentation for the import tag for more information):</p>

{% raw %}
<pre><code>{% import \"forms.html\" as forms %}
</code></pre>{% endraw %}

            <p>The above import call imports the \"forms.html\" file (which can contain only macros, or a template and some macros), and import the functions as items of the forms variable.</p>

            <p>The macro can then be called at will:</p>

{% raw %}
<pre><code>&lt;p&gt;{{ forms.input('username') }}&lt;/p&gt;
&lt;p&gt;{{ forms.input('password', null, 'password') }}&lt;/p&gt;
</code></pre>{% endraw %}

            <p>If macros are defined and used in the same template, you can use the special _self variable to import them:</p>

{% raw %}
<pre><code>{% import _self as forms %}
&lt;p&gt;{{ forms.input('username') }}&lt;/p&gt;
</code></pre>{% endraw %}

        </div>

    </div>

    <h3>TWIG FILTERS</h3>

    <div class=\"row\">

        <div class=\"span6\">

            <h4>date</h4>

            {% raw %}
<pre><code>{{ post.published_at|date(\"m/d/Y\") }}
{{ post.published_at|date(\"m/d/Y\", \"Europe/Paris\") }}
</code></pre>{% endraw %}

            <h4>date_modify</h4>

{% raw %}
<pre><code>{{ post.published_at|date_modify(\"+1 day\")|date(\"m/d/Y\") }}
</code></pre>{% endraw %}

            <h4>format</h4>

{% raw %}
<pre><code>{{ \"I like %s and %s.\"|format(foo, \"bar\") }}
</code></pre>{% endraw %}

            <h4>replace</h4>

{% raw %}
<pre><code>{{ \"I like %this% and %that%.\"|replace({'%this%': foo, '%that%': \"bar\"}
</code></pre>{% endraw %}

            <h4>number_format</h4>

{% raw %}
<pre><code>{{ 200.35|number_format }}
{{ 9800.333|number_format(2, '.', ',') }}
</code></pre>{% endraw %}

            <h4>url_encode</h4>

{% raw %}
<pre><code>{{ data|url_encode() }}
</code></pre>{% endraw %}

            <h4>json_encode</h4>

{% raw %}
<pre><code>{{ data|json_encode() }}
</code></pre>{% endraw %}

            <h4>convert_encoding</h4>

{% raw %}
<pre><code>{{ data|convert_encoding('UTF-8', 'iso-2022-jp') }}
</code></pre>{% endraw %}

            <h4>title</h4>

{% raw %}
<pre><code>{{ 'my first car'|title }}
{# outputs 'My First Car' #}
</code></pre>{% endraw %}

            <h4>capitalize</h4>

{% raw %}
<pre><code>{{ 'my first car'|capitalize }}
</code></pre>{% endraw %}

            <h4>nl2br</h4>

{% raw %}
<pre><code>{{ \"I like Twig.\\nYou will like it too.\"|nl2br }}
    {# outputs

    I like Twig.&lt;br /&gt;
    You will like it too.

    #}
</code></pre>{% endraw %}

            <h4>raw</h4>

{% raw %}
<pre><code>{{ var|raw }} {# var won't be escaped #}
</code></pre>{% endraw %}

            <h4>trim</h4>

{% raw %}
<pre><code>{{ ' I like Twig.'|trim('.') }}
</code></pre>{% endraw %}
        </div>

        <div class=\"span6\">
            <h4>upper</h4>

{% raw %}
<pre><code>{{ 'welcome'|upper }}
</code></pre>{% endraw %}

            <h4>lower</h4>

{% raw %}
<pre><code>{{ 'WELCOME'|lower }}
</code></pre>{% endraw %}

            <h4>striptags</h4>

{% raw %}
<pre><code>{% some_html|striptags %}
</code></pre>{% endraw %}

            <h4>join</h4>

{% raw %}
<pre><code>{{ [1, 2, 3]|join('|') }}
{# returns 1|2|3 #}
</code></pre>{% endraw %}

            <h4>split</h4>

{% raw %}
<pre><code>{{ \"one,two,three\"|split(',') }}
{# returns ['one', 'two', 'three'] #}
</code></pre>{% endraw %}

            <h4>reverse</h4>

{% raw %}
<pre><code>{% for use in users|reverse %}
    ...
{% endfor %}
</code></pre>{% endraw %}

            <h4>abs</h4>

{% raw %}
<pre><code>{{ number|abs }}
</code></pre>{% endraw %}

            <h4>length</h4>

{% raw %}
<pre><code>{% if users|length &gt; 10 %}
    ...
{% endif %}
</code></pre>{% endraw %}

            <h4>sort</h4>

{% raw %}
<pre><code>{% for use in users|sort %}
    ...
{% endfor %}
</code></pre>{% endraw %}

            <h4>default</h4>

{% raw %}
<pre><code>{{ var|default('var is not defined') }}
</code></pre>{% endraw %}

            <h4>keys</h4>

{% raw %}
<pre><code>{% for key in array|keys %}
    ...
{% endfor %}
</code></pre>{% endraw %}

            <h4>escape</h4>

{% raw %}
<pre><code>{{ user.username|e }}
{# is equivalent to #}
{{ user.username|e('html') }}
{{ user.username|e('css') }}
{{ user.username|e('js') }}
</code></pre>{% endraw %}

        </div>

    </div>

{% endblock %}
", "@E01/Default/cat_templating.html.twig", "/home/dw31/cur_projects/Symfony-Pool/d04/src/E01Bundle/Resources/views/Default/cat_templating.html.twig");
    }
}
