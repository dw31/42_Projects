<?php

/* @E01/Default/cat_routing.html.twig */
class __TwigTemplate_59463cdfc0687a69c27949009daae9273809a779310fbe2a84547e65420889d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@E01/base.html.twig", "@E01/Default/cat_routing.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@E01/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@E01/Default/cat_routing.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@E01/Default/cat_routing.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 3
        echo "

            <p>Routing in Symfony 2 is even easier than in Symfony 1.x. Here is an example of the most complex routing you can get in Symfony 2.</p>

";
        // line 16
        echo "
<pre><code>article_show:
    pattern: /{_locale}/article-details/{page}.{_format}
    defaults: {_controller:Bundle:Controller:Action, _format: html, page:1}
    requirements:
        _locale: en|fr
        _format: html|rss
        page: \\d+
        _scheme: http|https
</code></pre>";
        echo "
            <p>Also you can prefix imported routes and give a group of routes a prepend text:</p>

";
        // line 24
        echo "
<pre><code>#app/config/routing.yml
    acme_hello:
    resource: \"@AcmeHelloBundle/Resources/config/routing.yml\"
    prefix: /admin
</code></pre>";
        echo "

            <h4>Working with annotations</h4>
<p>You can use <strong>annotations</strong> in your controller by enabling annotations in your <strong>routing.yml</strong> and in your <strong>config.yml</strong></p>

<pre><code>#config.yml
sensio_framework_extra:
    router:  { annotations: true }
    request: { converters: true }
    view:    { annotations: true }
    cache:   { annotations: true }
</code></pre>

<pre><code>#routing.yml
acmedemo_main:
    resource: \"@AcmeDemoWebBundle/Controller\"
    prefix:   /
    type: annotation
</code></pre>

            <p>In your controller:</p>
<pre><code>/**
* @Route(\"/{_locale}/\", name=\"localizedHomepage\")
* @Method(\"POST\")
* @param \$name
* @Template(\"AcmeDemoWebBundle:Web:index.html.twig\")
* @Cache(expires=\"+30 days\")
*/
public function localizedHomepageAction(\$name)
{
    return array('name' => \$name);
}
</code></pre>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@E01/Default/cat_routing.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 24,  55 => 16,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@E01/base.html.twig' %}
{% block content %}


            <p>Routing in Symfony 2 is even easier than in Symfony 1.x. Here is an example of the most complex routing you can get in Symfony 2.</p>

{% raw %}
<pre><code>article_show:
    pattern: /{_locale}/article-details/{page}.{_format}
    defaults: {_controller:Bundle:Controller:Action, _format: html, page:1}
    requirements:
        _locale: en|fr
        _format: html|rss
        page: \\d+
        _scheme: http|https
</code></pre>{% endraw %}
            <p>Also you can prefix imported routes and give a group of routes a prepend text:</p>

{% raw %}
<pre><code>#app/config/routing.yml
    acme_hello:
    resource: \"@AcmeHelloBundle/Resources/config/routing.yml\"
    prefix: /admin
</code></pre>{% endraw %}

            <h4>Working with annotations</h4>
<p>You can use <strong>annotations</strong> in your controller by enabling annotations in your <strong>routing.yml</strong> and in your <strong>config.yml</strong></p>

<pre><code>#config.yml
sensio_framework_extra:
    router:  { annotations: true }
    request: { converters: true }
    view:    { annotations: true }
    cache:   { annotations: true }
</code></pre>

<pre><code>#routing.yml
acmedemo_main:
    resource: \"@AcmeDemoWebBundle/Controller\"
    prefix:   /
    type: annotation
</code></pre>

            <p>In your controller:</p>
<pre><code>/**
* @Route(\"/{_locale}/\", name=\"localizedHomepage\")
* @Method(\"POST\")
* @param \$name
* @Template(\"AcmeDemoWebBundle:Web:index.html.twig\")
* @Cache(expires=\"+30 days\")
*/
public function localizedHomepageAction(\$name)
{
    return array('name' => \$name);
}
</code></pre>
{% endblock %}
", "@E01/Default/cat_routing.html.twig", "/home/dw31/cur_projects/Symfony-Pool/d04/src/E01Bundle/Resources/views/Default/cat_routing.html.twig");
    }
}
