<?php

/* @E01/Default/cat_services.html.twig */
class __TwigTemplate_4bf6e649473d9708abea2e8354840c419a91cc37ae48580a8c5a149b4fbdbabf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@E01/base.html.twig", "@E01/Default/cat_services.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@E01/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@E01/Default/cat_services.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@E01/Default/cat_services.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 4
        echo "


    <p>A Service Container (or dependency injection container) is simply a PHP object that manages the instantiation of services (i.e. objects).</p>

";
        // line 29
        echo "<pre><code># src/Acme/HelloBundle/Resources/config/services.yml
parameters:
    newsletter_manager.class: Acme\\HelloBundle\\Newsletter\\NewsletterManager
    my_mailer.transport: sendmail
    my_mailer.gateways:
        - mail1
        - mail2
        - mail3

services:
    my_mailer:
        class: \"%my_mailer.class%\"
        arguments: [%my_mailer.transport%]
    newsletter_manager:
        class: \"%newsletter_manager.class%\"
        arguments: [@my_mailer] //required contructor args. Use @ to refer another service.
        calls:
        - [ setMailer, [ @my_mailer ] ] //Optional dependencies.
        tags:
        - { name: twig.extension } //Twig finds all services tagged with twig.extension and automatically registers them as extensions.
</code></pre>";
        echo "

    <p>Now we can set our class to be a real service:</p>

";
        // line 50
        echo "<pre><code>namespace Acme\\HelloBundle\\Newsletter;

use Symfony\\Component\\Templating\\EngineInterface;

class NewsletterManager
{
    protected \$mailer;
    protected \$templating;

    public function __construct(\\Swift_Mailer \$mailer, EngineInterface \$templating)
    {
        \$this-&gt;mailer = \$mailer;
        \$this-&gt;templating = \$templating;
    }

    // ...
}
</code></pre>";
        echo "

    <p>And for this particual service the corresponding services.yml would be:</p>

";
        // line 58
        echo "<pre><code> services:
    newsletter_manager:
        class: \"%newsletter_manager.class%\"
        arguments: [@mailer, @templating]
</code></pre>";
        echo "

    <p>In YAML, the special @my_mailer syntax tells the container to look for a service named my_mailer and to pass that object into the constructor of NewsletterManager. In this case, however, the specified service my_mailer must exist. If it does not, an exception will be thrown. You can mark your dependencies as optional - this will be discussed in the next section</p>

    <p><strong>Making References Optional</strong></p>

";
        // line 68
        echo "<pre><code>services:
    newsletter_manager:
        class: \"%newsletter_manager.class%\"
        arguments: [@?my_mailer]
</code></pre>";
        echo "

    <h3>Debugging services</h3>

    <p>You can find out what services are registered with the container using the console. To show all services and the class for each service, run:</p>

";
        // line 77
        echo "<pre><code>\$ php app/console container:debug
\$ php app/console container:debug --show-private
\$ php app/console container:debug my_mailer
</code></pre>";
        echo "

    <p>See also:</p>

    <ul>
        <li>
            <a href=\"http://symfony.com/doc/current/components/dependency_injection/parentservices.html\">Managing Common Dependencies with Parent Services</a>
        </li>
    </ul>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@E01/Default/cat_services.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 77,  120 => 68,  107 => 58,  83 => 50,  56 => 29,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@E01/base.html.twig' %}

{% block content %}



    <p>A Service Container (or dependency injection container) is simply a PHP object that manages the instantiation of services (i.e. objects).</p>

{% raw %}<pre><code># src/Acme/HelloBundle/Resources/config/services.yml
parameters:
    newsletter_manager.class: Acme\\HelloBundle\\Newsletter\\NewsletterManager
    my_mailer.transport: sendmail
    my_mailer.gateways:
        - mail1
        - mail2
        - mail3

services:
    my_mailer:
        class: \"%my_mailer.class%\"
        arguments: [%my_mailer.transport%]
    newsletter_manager:
        class: \"%newsletter_manager.class%\"
        arguments: [@my_mailer] //required contructor args. Use @ to refer another service.
        calls:
        - [ setMailer, [ @my_mailer ] ] //Optional dependencies.
        tags:
        - { name: twig.extension } //Twig finds all services tagged with twig.extension and automatically registers them as extensions.
</code></pre>{% endraw %}

    <p>Now we can set our class to be a real service:</p>

{% raw %}<pre><code>namespace Acme\\HelloBundle\\Newsletter;

use Symfony\\Component\\Templating\\EngineInterface;

class NewsletterManager
{
    protected \$mailer;
    protected \$templating;

    public function __construct(\\Swift_Mailer \$mailer, EngineInterface \$templating)
    {
        \$this-&gt;mailer = \$mailer;
        \$this-&gt;templating = \$templating;
    }

    // ...
}
</code></pre>{% endraw %}

    <p>And for this particual service the corresponding services.yml would be:</p>

{% raw %}<pre><code> services:
    newsletter_manager:
        class: \"%newsletter_manager.class%\"
        arguments: [@mailer, @templating]
</code></pre>{% endraw %}

    <p>In YAML, the special @my_mailer syntax tells the container to look for a service named my_mailer and to pass that object into the constructor of NewsletterManager. In this case, however, the specified service my_mailer must exist. If it does not, an exception will be thrown. You can mark your dependencies as optional - this will be discussed in the next section</p>

    <p><strong>Making References Optional</strong></p>

{% raw %}<pre><code>services:
    newsletter_manager:
        class: \"%newsletter_manager.class%\"
        arguments: [@?my_mailer]
</code></pre>{% endraw %}

    <h3>Debugging services</h3>

    <p>You can find out what services are registered with the container using the console. To show all services and the class for each service, run:</p>

{% raw %}<pre><code>\$ php app/console container:debug
\$ php app/console container:debug --show-private
\$ php app/console container:debug my_mailer
</code></pre>{% endraw %}

    <p>See also:</p>

    <ul>
        <li>
            <a href=\"http://symfony.com/doc/current/components/dependency_injection/parentservices.html\">Managing Common Dependencies with Parent Services</a>
        </li>
    </ul>
{% endblock %}
", "@E01/Default/cat_services.html.twig", "/home/dw31/cur_projects/Symfony-Pool/d04/src/E01Bundle/Resources/views/Default/cat_services.html.twig");
    }
}
