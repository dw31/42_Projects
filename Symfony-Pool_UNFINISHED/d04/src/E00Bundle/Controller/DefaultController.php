<?php

namespace E00Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/e00/firstpage/", name="e00firstpage")
     */
    public function e00firstpageAction()
    {
        return new Response('Hello world!');
    }
}
