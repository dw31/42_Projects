<?php

namespace E01Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    private $categories = array('controller', 'routing', 'templating', 'doctrine', 'testing', 'validation', 'forms', 'security', 'cache', 'translations', 'services');

    /**
     * @Route("/e01/")
     */
    public function indexAction()
    {
        return $this->render('@E01/Default/index.html.twig', array('categories' => $this->categories));
    }

    /**
     * @Route("/e01/{category}/")
     */
    public function categoryAction($category)
    {
        if (in_array($category, $this->categories) === FALSE)
            return ($this->indexAction());
        return $this->render('@E01/Default/cat_'.$category.'.html.twig');
    }
}
