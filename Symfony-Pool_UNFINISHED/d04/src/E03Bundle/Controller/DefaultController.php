<?php

namespace E03Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/e03/")
     */
    public function indexAction()
    {
        return $this->render('@E03/Default/index.html.twig', array ('colors' => array('red', 'black')));
    }
}
