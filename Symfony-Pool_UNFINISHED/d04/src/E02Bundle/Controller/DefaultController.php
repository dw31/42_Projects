<?php

namespace E02Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use \Datetime;
use Symfony\Component\HttpKernel\Kernel;


class DefaultController extends Controller
{
    /**
     * try to open file, if failed return NULL otherwise return last line
     */
    private function file_getLastLine()
    {
        $fs= new Filesystem();
        if ($fs->exists($this->get('kernel')->getRootDir().'/../'.$this->getParameter('e02filename')))
        {    
            $str = file_get_contents($this->get('kernel')->getRootDir().'/../'.$this->getParameter('e02filename'));
            return (strrchr(file_get_contents($this->get('kernel')->getRootDir().'/../'.$this->getParameter('e02filename')), PHP_EOL));
        }
        return ('no message');
    }

    /**
     * open or create file then append the message (prefixed by ts if enabled)
     */
    private function file_putMessage($message)
    {
        $fs= new Filesystem();
        if ($fs->exists($this->get('kernel')->getRootDir().'/../'.$this->getParameter('e02filename')) === FALSE)
            $fs->dumpFile($this->get('kernel')->getRootDir().'/../'.$this->getParameter('e02filename'), $message.PHP_EOL);
        else
            $fs->appendToFile($this->get('kernel')->getRootDir().'/../'.$this->getParameter('e02filename'), $message.PHP_EOL);
    }

    /**
     * @Route("/e02/")
     */
    public function indexAction(Request $request)
    {
        $message = NULL;
        $form = $this->createFormBuilder()
            ->add('message', TextType::class, array('required' => false))
            ->add('includeTs', ChoiceType::class, array('choices' => array('Yes' => true, 'No' => false),))
            ->add('submit', SubmitType::class, array('label' => 'Submit'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $form->getData()['message'] !== NULL)
        {
            $message = $form->getData()['message'];
            if ($form->getData()['includeTs'])
            {
                $ts = new DateTime();
                $message = '['.$ts->getTimestamp().'] '.$message;
            }
            $this->file_putMessage($message);
        }

        return $this->render('@E02/Default/index.html.twig', array('form' => $form->createView(), 'lastMessage' => $message));
    }
}
