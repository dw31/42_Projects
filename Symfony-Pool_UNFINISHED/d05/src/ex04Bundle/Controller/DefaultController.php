<?php

namespace ex04Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use \PDO;

class DefaultController extends Controller
{
    /**
     * @Route("/ex04/")
     */
    public function indexAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        $db->query('CREATE TABLE IF NOT EXISTS `ex04` (
            `id` int(11) AUTO_INCREMENT PRIMARY KEY,
            `username` VARCHAR(255) UNIQUE,
            `name` VARCHAR(255),
            `email` VARCHAR(255) UNIQUE,
            `enable` BOOLEAN,
            `birthdate` DATETIME,
            `address` LONGTEXT)');

        $db->query('use '.$this->getParameter('database_name'));
        $query = $db->prepare(' SELECT `id`, `username`, `name`, `email`, `enable`, `birthdate`, `address` FROM `ex04` ');
        $query->execute();

        return $this->render('@ex04/Default/index.html.twig', array('ret' => $query->fetchAll()));
    }

    /**
     * @Route("/ex04/delete/{id}")
     */
    public function deleteAction($id)
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));
        $db->query('use '.$this->getParameter('database_name'));
     
        $ret = 0;
        $query = $db->prepare(' SELECT count(*) FROM `ex04` WHERE `id` = ? ');
        $query->bindValue(1, $id, PDO::PARAM_INT);
        $query->bindColumn(1, $ret);
        $query->execute();
        $query->fetch();
        if (!$ret)
            return $this->render('@ex04/Default/delete.html.twig', array('msg' => 'Invalid id')); 
    
        $db->query(' DELETE FROM `ex04` WHERE `id` = '.$id.' ');
        return $this->render('@ex04/Default/delete.html.twig', array('msg' => 'Id '.$id.' deleted')); 
    }
}
