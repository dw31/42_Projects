<?php

namespace ex06Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \PDO;

class DefaultController extends Controller
{
    /**
     * @Route("/ex06/")
     */
     public function indexAction()
     {
         $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));
 
         $db->query('use '.$this->getParameter('database_name'));
         $db->query('CREATE TABLE IF NOT EXISTS `ex06` (
             `id` int(11) AUTO_INCREMENT PRIMARY KEY,
             `username` VARCHAR(255) UNIQUE,
             `name` VARCHAR(255),
             `email` VARCHAR(255) UNIQUE,
             `enable` BOOLEAN,
             `birthdate` DATETIME,
             `address` LONGTEXT)');
 
         $db->query('use '.$this->getParameter('database_name'));
         $query = $db->prepare(' SELECT `id`, `username`, `name`, `email`, `enable`, `birthdate`, `address` FROM `ex06` ');
         $query->execute();
 
         return $this->render('@ex06/Default/index.html.twig', array('ret' => $query->fetchAll()));
     }

    /**
     * @Route("/ex06/update/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));
        $db->query('use '.$this->getParameter('database_name'));

        $ret = 0; 
        $query = $db->prepare(' SELECT count(*) FROM `ex06` WHERE `id` = ? ');
        $query->bindValue(1, $id, PDO::PARAM_INT);
        $query->bindColumn(1, $ret);
        $query->execute();
        $query->fetch();
        if (!$ret)
            return $this->render('@ex06/Default/update.html.twig', array('msg' => 'Invalid id'.$id)); 

        $form = $this->createFormBuilder()
        ->add('username', TextType::class, array('required' => TRUE))
        ->add('name', TextType::class, array('required' => FALSE))
        ->add('email', EmailType::class, array('required' => TRUE))
        ->add('enable', CheckboxType::class, array('required' => FALSE))
        ->add('birthdate', DateTimeType::class, array('required' => FALSE, 'choice_translation_domain' => 'string', 'date_widget' => 'single_text'))
        ->add('address', TextType::class, array('required' => FALSE))
        ->add('submit', SubmitType::class, array('label' => 'Update User'))
        ->getForm();

        $query = $db->prepare(' SELECT `username`, `name`, `email`, `enable`, `birthdate`, `address` FROM `ex06` WHERE `id` = ? ');
        $query->bindValue(1, $id, PDO::PARAM_INT);
        $query->execute();

        $ret = $query->fetch(PDO::FETCH_ASSOC);
        $arr = array();
        $arr['username'] = $ret['username'];
        $arr['name'] = $ret['name'];
        $arr['email'] = $ret['email'];
        if ($ret['enable'] !== NULL)
            $arr['enable'] = ($ret['enable']) ? true : false;
        if ($ret['birthdate'] !== NULL)
            $arr['birthdate'] = date_create($ret['birthdate']);
        $arr['address'] = $ret['address'];

        $form->setData($arr); 

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $db->query('use '.$this->getParameter('database_name'));
            $query = $db->prepare(' UPDATE `ex06` SET `username` = ?, `name` = ?, `email` = ?, `enable` = ?, `birthdate` = ?, `address` = ? WHERE `id` = ? ');
            $query->bindValue(1, $form->get('username')->getData(), PDO::PARAM_STR);
            $query->bindValue(2, $form->get('name')->getData(), PDO::PARAM_STR);
            $query->bindValue(3, $form->get('email')->getData(), PDO::PARAM_STR);
            $query->bindValue(4, $form->get('enable')->getData(), PDO::PARAM_STR);
            $query->bindValue(5, $form->get('birthdate')->getData()->format('Y-m-d H:i:s'), PDO::PARAM_STR);
            $query->bindValue(6, $form->get('address')->getData(), PDO::PARAM_STR);
            $query->bindValue(7, $id, PDO::PARAM_INT);
            $query->execute();

            $arr['username'] = $form->get('username')->getData();
            $arr['name'] = $form->get('name')->getData();
            $arr['email'] = $form->get('email')->getData();
            $arr['enable'] = $form->get('enable')->getData();
            $arr['birthdate'] = $form->get('birthdate')->getData();
            $arr['address'] = $form->get('address')->getData();

            return $this->render('@ex06/Default/update.html.twig', array('form' => $form->createView(), 'msg' => 'Id '.$id.' updated')); 
        }
        return $this->render('@ex06/Default/update.html.twig', array('form' => $form->createView(), 'msg' => NULL));
    }
}
