<?php

namespace ex00Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/ex00/createit/")
     */
     public function createitAction()
     {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));
        $db->query('use '.$this->getParameter('database_name'));
        $db->query('CREATE TABLE IF NOT EXISTS `ex00` (
            `id` int(11) AUTO_INCREMENT PRIMARY KEY,
            `username` VARCHAR(255) UNIQUE,
            `name` VARCHAR(255),
            `email` VARCHAR(255) UNIQUE,
            `enable` BOOLEAN,
            `birthdate` DATETIME,
            `address` LONGTEXT)');
        return $this->render('@ex00\Default\index.html.twig', array('ret'=> 'success'));
     }

    /**
     * @Route("/ex00/")
     */
    public function indexAction()
    {
         return $this->render('@ex00\Default\index.html.twig', array('ret'=> NULL));
    }
}
