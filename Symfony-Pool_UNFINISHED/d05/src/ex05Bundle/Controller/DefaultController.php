<?php

namespace ex05Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\Mapping as ORM;
use ex05Bundle\Entity\ex05;
use ex05Bundle\Repository;

class DefaultController extends Controller
{
    /**
     * @Route("/ex05/")
     */
    public function indexAction()
    {
        return $this->render('@ex05/Default/index.html.twig', array('ret' => $this->getDoctrine()->getRepository(Ex05::class)->findAll()));
    }

    /**
     * @Route("/ex05/delete/{id}")
     */
    public function deleteAction($id)
    {
        $ent = $this->getDoctrine()->getRepository(Ex05::class)->find($id);
        if ($ent === NULL)
            return $this->render('@ex05/Default/delete.html.twig', array('msg' => 'Invalid id')); 
        $em = $this->getDoctrine()->getManager();
        $em->remove($ent);
        $em->flush();
        return $this->render('@ex05/Default/delete.html.twig', array('msg' => 'Id '.$id.' deleted')); 
    }
}
