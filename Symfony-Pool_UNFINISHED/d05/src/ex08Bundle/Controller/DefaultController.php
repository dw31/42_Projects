<?php

namespace ex08Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use \PDO;

class DefaultController extends Controller
{
    /**
     * @Route("/ex08/")
     */
    public function indexAction()
    {
        return $this->render('@ex08\Default\index.html.twig', array('msg' => NULL));
    }

    /**
     * @Route("/ex08/createTablePersons")
     */
    public function createTablePersonsAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        $db->query('CREATE TABLE IF NOT EXISTS `persons` (
            `id` int(11) AUTO_INCREMENT PRIMARY KEY,
            `username` VARCHAR(255) UNIQUE,
            `name` VARCHAR(255),
            `email` VARCHAR(255) UNIQUE,
            `enable` BOOLEAN,
            `birthdate` DATETIME)');
        return $this->render('@ex08\Default\index.html.twig', array('msg' => 'Table \'persons\' created'));
    }

    /**
     * @Route("/ex08/addMaritalColumn")
     */
    public function addMaritalColumnAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        $db->query('ALTER TABLE `persons` ADD COLUMN `marital_status` ENUM("single", "married", "widower")');
        return $this->render('@ex08\Default\index.html.twig', array('msg' => 'Column \'marital_status\' added to \'persons\''));
  }

    /**
     * @Route("/ex08/createTableBankAccounts")
     */
    public function createTableBankAccountsAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        if ($db->query('CREATE TABLE IF NOT EXISTS `bank_accounts` (
            `id` int(11) AUTO_INCREMENT PRIMARY KEY,
            `personsId` INT(11) UNIQUE,
            `ammount` INT(11))'))
          return $this->render('@ex08\Default\index.html.twig', array('msg' => 'Table \'bank_accounts\' created'));
        return $this->render('@ex08\Default\index.html.twig', array('msg' => 'ERROR'));
    }

    /**
     * @Route("/ex08/createTableAddresses")
     */
    public function createTableAddressesAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        if ($db->query('CREATE TABLE IF NOT EXISTS `addresses` (
            `id` int(11) AUTO_INCREMENT PRIMARY KEY,
            `address` VARCHAR(255) UNIQUE)'))
            return $this->render('@ex08\Default\index.html.twig', array('msg' => 'Table \'addresses\' created'));
        return $this->render('@ex08\Default\index.html.twig', array('msg' => 'ERROR'));
    }

    /**
     * @Route("/ex08/linkBankAccountsToPersons")
     */
    public function linkBankAccountsToPersonsAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        if ($ret = $db->query('ALTER TABLE `bank_accounts` ADD CONSTRAINT FK_PersonsBankAccounts FOREIGN KEY (personsId) REFERENCES persons(id)'))
            return $this->render('@ex08\Default\index.html.twig', array('msg' => '\'bank_accounts\' linked to \'persons\''));
        return $this->render('@ex08\Default\index.html.twig', array('msg' => 'ERROR'));
    }

    /**
     * @Route("/ex08/linkAddressesToPersons")
     */
    public function linkAddressesToPersonsAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        $db->query('use '.$this->getParameter('database_name'));
        if (!$db->query('ALTER TABLE `persons` ADD COLUMN `addressesId` INT(11)'))
          return $this->render('@ex08\Default\index.html.twig', array('msg' => 'ERROR'));
        if (!$db->query('ALTER TABLE `persons` ADD CONSTRAINT FK_PersonsAddresses FOREIGN KEY (addressesId) REFERENCES addresses(id)'))
          return $this->render('@ex08\Default\index.html.twig', array('msg' => 'ERROR'));
        return $this->render('@ex08\Default\index.html.twig', array('msg' => '\'persons\' linked to \'addresses\''));
    }
}
