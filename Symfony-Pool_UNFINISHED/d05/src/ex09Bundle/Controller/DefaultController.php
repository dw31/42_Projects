<?php

namespace ex09Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Doctrine\ORM\Mapping as ORM;
//use ex07Bundle\Entity\ex07;
//use ex07Bundle\Repository;

class DefaultController extends Controller
{
    /**
     * @Route("/ex09/")
     */
    public function indexAction()
    {
        return $this->render('@ex09\Default\index.html.twig', array('msg' => NULL));
    }

    /**
     * @Route("/ex09/createTables")
     */
    public function createTablesAction()
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(array('command' => 'doctrine:schema:update', '--force' => true));
        $application->run($input, new NullOutput());
        return $this->render('@ex09\Default\index.html.twig', array('msg' => ''));
    }

    /**
     * @Route("/ex09/linkTables")
     */
    public function linkTablesAction()
    {
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(array('command' => 'doctrine:schema:update', '--force' => true));
        $application->run($input, new NullOutput());
        return $this->render('@ex09\Default\index.html.twig', array('msg' => NULL));
    }
}
