<?php

namespace ex02Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use \PDO; 

class DefaultController extends Controller
{
    /**
     * @Route("/ex02/")
     */
    public function indexAction(Request $request)
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));

        // create table if not exists
            $db->query('use '.$this->getParameter('database_name'));
            $db->query('CREATE TABLE IF NOT EXISTS `ex02` (
                `id` int(11) AUTO_INCREMENT PRIMARY KEY,
                `username` VARCHAR(255) UNIQUE,
                `name` VARCHAR(255),
                `email` VARCHAR(255) UNIQUE,
                `enable` BOOLEAN,
                `birthdate` DATETIME,
                `address` LONGTEXT)');
        //

        $form = $this->createFormBuilder()
        ->add('username', TextType::class, array('required' => TRUE))
        ->add('name', TextType::class, array('required' => FALSE))
        ->add('email', EmailType::class, array('required' => TRUE))
        ->add('enable', CheckboxType::class, array('required' => FALSE))
        ->add('birthdate', DateTimeType::class, array('required' => FALSE, 'choice_translation_domain' => 'string'))
        ->add('address', TextType::class, array('required' => FALSE))
        ->add('submit', SubmitType::class, array('label' => 'Create User'))
        ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $db->query('use '.$this->getParameter('database_name'));
            
            $query = $db->prepare(' SELECT count(*) FROM `ex02` WHERE `username` = ? OR `email` = ? ');
            $query->bindValue(1, $form->get('username')->getData(), PDO::PARAM_STR);
            $query->bindValue(2, $form->get('email')->getData(), PDO::PARAM_STR);
            $query->bindColumn(1, $ret);
            $query->execute();
            $query->fetch();
            if ($ret)
                return $this->render('@ex02/Default/index.html.twig', array('ret'=> NULL, 'form' => $form->createView(), 'msg' => 'User already exists'));        

            $query = $db->prepare(' INSERT INTO `ex02` (`username`, `name`, `email`, `enable`, `birthdate`, `address`) VALUES (?,?,?,?,?,?); ');
            $query->bindValue(1, $form->get('username')->getData(), PDO::PARAM_STR);
            if ($form->get('name')->getData())
                $query->bindValue(2, $form->get('name')->getData(), PDO::PARAM_STR);
            else
                $query->bindValue(2, NULL, PDO::PARAM_NULL);              
            $query->bindValue(3, $form->get('email')->getData(), PDO::PARAM_STR);
            $query->bindValue(4, $form->get('enable')->getData(), PDO::PARAM_BOOL);
            if ($form->get('birthdate')->getData())
                $query->bindValue(5, date_format($form->get('birthdate')->getData(), 'Y-m-d H:i:s'), PDO::PARAM_STR);
            else
                $query->bindValue(5, NULL, PDO::PARAM_NULL); 
            if ($form->get('address')->getData())
                $query->bindValue(6, $form->get('address')->getData(), PDO::PARAM_STR);
            else
                $query->bindValue(6, NULL, PDO::PARAM_NULL); 
            $query->execute();
            return $this->render('@ex02/Default/index.html.twig', array('ret'=> NULL, 'form' => $form->createView(), 'msg' => 'User added'));        
        }
        
        return $this->render('@ex02/Default/index.html.twig', array('ret'=> NULL, 'form' => $form->createView(), 'msg' => NULL));
    }

    /**
     * @Route("/ex02/display")
     */
    public function displayAction()
    {
        $db = new \PDO("mysql:host=".$this->getParameter('database_host'), $this->getParameter('database_user'), $this->getParameter('database_password'));
        $db->query('use '.$this->getParameter('database_name'));
        $query = $db->prepare(' SELECT `id`, `username`, `name`, `email`, `enable`, `birthdate`, `address` FROM `ex02` ');
        $query->execute();
        return $this->render('@ex02/Default/display.html.twig', array('ret' => $query->fetchAll()));
    }
}
