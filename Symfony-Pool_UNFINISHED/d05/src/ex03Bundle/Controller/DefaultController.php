<?php

namespace ex03Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\EntityManagerInterface;
use ex03Bundle\Entity\ex03;
use ex03Bundle\Repository;

class DefaultController extends Controller
{
    /**
     * @Route("/ex03/")
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
        ->add('username', TextType::class, array('required' => TRUE))
        ->add('name', TextType::class, array('required' => FALSE))
        ->add('email', EmailType::class, array('required' => TRUE))
        ->add('enable', CheckboxType::class, array('required' => FALSE))
        ->add('birthdate', DateTimeType::class, array('required' => FALSE, 'choice_translation_domain' => 'string'))
        ->add('address', TextType::class, array('required' => FALSE))
        ->add('submit', SubmitType::class, array('label' => 'Create User'))
        ->getForm();
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            
            $repo = $this->getDoctrine()->getRepository(Ex03::class);
            if ($repo->findByUsername($form->get('username')->getData()) || $repo->findByEmail($form->get('email')->getData()))
                return $this->render('@ex03/Default/index.html.twig', array('ret'=> NULL, 'form' => $form->createView(), 'msg' => 'User already exists'));
            
            $ex03 = new Ex03();
            $ex03->setUsername($form->get('username')->getData());
            if ($form->get('name')->getData())
                $ex03->setName($form->get('name')->getData());
            $ex03->setEmail($form->get('email')->getData());
            if ($form->get('enable')->getData())
                $ex03->setEnable($form->get('enable')->getData());
            if ($form->get('birthdate')->getData())
                $ex03->setBirthdate($form->get('birthdate')->getData());
            if ($form->get('address')->getData())
                $ex03->setAddress($form->get('address')->getData());
            $em->persist($ex03);
            $em->flush();
            return $this->render('@ex03/Default/index.html.twig', array('ret'=> NULL, 'form' => $form->createView(), 'msg' => 'User added'));        
        }

        return $this->render('@ex03/Default/index.html.twig', array('ret'=> NULL, 'form' => $form->createView(), 'msg' => NULL));
    }
    
    /**
     * @Route("/ex03/display")
     */
    public function displayAction()
    {
        $repo = $this->getDoctrine()->getRepository(Ex03::class);
        return $this->render('@ex03/Default/display.html.twig', array('ret' => $repo->findAll()));
    }
}