<?php

namespace ex01Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class DefaultController extends Controller
{
    /**
     * @Route("/ex01/")
     */
    public function indexAction()
    {
        return $this->render('@ex01/Default/index.html.twig', array('ret'=> NULL));
    }

    /**
     * @Route("/ex01/createit")
     */
     public function createitAction()
     {
        // https://symfony.com/doc/3.4/console/command_in_controller.html
        $kernel = $this->get('kernel');
        $application = new Application($kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(array('command' => 'doctrine:schema:update', '--force' => true));
        $application->run($input, new NullOutput());
        return $this->render('@ex01/Default/index.html.twig', array('ret'=> 'success'));
     }
}

