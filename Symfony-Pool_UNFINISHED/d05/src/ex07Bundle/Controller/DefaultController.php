<?php

namespace ex07Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\ORM\Mapping as ORM;
use ex07Bundle\Entity\ex07;
use ex07Bundle\Repository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{
    /**
     * @Route("/ex07/")
     */
    public function indexAction()
    {
        return $this->render('@ex07/Default/index.html.twig', array('ret' => $this->getDoctrine()->getRepository(Ex07::class)->findAll()));
    }

    /**
     * @Route("/ex07/update/{id}")
     */
    public function updateAction($id, Request $request)
    {
        $ent = $this->getDoctrine()->getRepository(Ex07::class)->find($id);
        if ($ent === NULL)
            return $this->render('@ex07/Default/update.html.twig', array('msg' => 'Invalid id'.$id)); 
    
        $form = $this->createFormBuilder()
        ->add('username', TextType::class, array('required' => TRUE))
        ->add('name', TextType::class, array('required' => FALSE))
        ->add('email', EmailType::class, array('required' => TRUE))
        ->add('enable', CheckboxType::class, array('required' => FALSE))
        ->add('birthdate', DateTimeType::class, array('required' => FALSE, 'choice_translation_domain' => 'string', 'date_widget' => 'single_text'))
        ->add('address', TextType::class, array('required' => FALSE))
        ->add('submit', SubmitType::class, array('label' => 'Update User'))
        ->getForm();
    
        $form->setData($ent->getEntityAsArray());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $ent->setEntityWithArray($form->getData());
            $em = $this->getDoctrine()->getManager();
            $em->persist($ent);
            $em->flush();
            return $this->render('@ex07/Default/update.html.twig', array('form' => $form->createView(), 'msg' => 'Id '.$id.' updated'));         
        }
        return $this->render('@ex07/Default/update.html.twig', array('form' => $form->createView(), 'msg' => NULL));
    }
}
