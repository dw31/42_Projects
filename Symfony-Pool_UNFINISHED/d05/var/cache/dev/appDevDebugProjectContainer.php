<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\Container7pj37jr\appDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/Container7pj37jr/appDevDebugProjectContainer.php') {
    touch(__DIR__.'/Container7pj37jr.legacy');

    return;
}

if (!\class_exists(appDevDebugProjectContainer::class, false)) {
    \class_alias(\Container7pj37jr\appDevDebugProjectContainer::class, appDevDebugProjectContainer::class, false);
}

return new \Container7pj37jr\appDevDebugProjectContainer(array(
    'container.build_hash' => '7pj37jr',
    'container.build_id' => '9abe1c0a',
    'container.build_time' => 1531374822,
), __DIR__.\DIRECTORY_SEPARATOR.'Container7pj37jr');
