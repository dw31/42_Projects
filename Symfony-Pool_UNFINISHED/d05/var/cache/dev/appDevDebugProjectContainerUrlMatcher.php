<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request ?: $this->createRequest($pathinfo);
        $requestMethod = $canonicalMethod = $context->getMethod();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not__profiler_home;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }
                not__profiler_home:

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#sD', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/ex0')) {
            if (0 === strpos($pathinfo, '/ex09')) {
                // ex09_default_index
                if ('/ex09' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex09Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex09_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex09_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex09_default_index'));
                    }

                    return $ret;
                }
                not_ex09_default_index:

                // ex09_default_createtables
                if ('/ex09/createTables' === $pathinfo) {
                    return array (  '_controller' => 'ex09Bundle\\Controller\\DefaultController::createTablesAction',  '_route' => 'ex09_default_createtables',);
                }

                // ex09_default_linktables
                if ('/ex09/linkTables' === $pathinfo) {
                    return array (  '_controller' => 'ex09Bundle\\Controller\\DefaultController::linkTablesAction',  '_route' => 'ex09_default_linktables',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ex08')) {
                // ex08_default_index
                if ('/ex08' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex08_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex08_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex08_default_index'));
                    }

                    return $ret;
                }
                not_ex08_default_index:

                if (0 === strpos($pathinfo, '/ex08/createTable')) {
                    // ex08_default_createtablepersons
                    if ('/ex08/createTablePersons' === $pathinfo) {
                        return array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::createTablePersonsAction',  '_route' => 'ex08_default_createtablepersons',);
                    }

                    // ex08_default_createtablebankaccounts
                    if ('/ex08/createTableBankAccounts' === $pathinfo) {
                        return array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::createTableBankAccountsAction',  '_route' => 'ex08_default_createtablebankaccounts',);
                    }

                    // ex08_default_createtableaddresses
                    if ('/ex08/createTableAddresses' === $pathinfo) {
                        return array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::createTableAddressesAction',  '_route' => 'ex08_default_createtableaddresses',);
                    }

                }

                // ex08_default_addmaritalcolumn
                if ('/ex08/addMaritalColumn' === $pathinfo) {
                    return array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::addMaritalColumnAction',  '_route' => 'ex08_default_addmaritalcolumn',);
                }

                // ex08_default_linkbankaccountstopersons
                if ('/ex08/linkBankAccountsToPersons' === $pathinfo) {
                    return array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::linkBankAccountsToPersonsAction',  '_route' => 'ex08_default_linkbankaccountstopersons',);
                }

                // ex08_default_linkaddressestopersons
                if ('/ex08/linkAddressesToPersons' === $pathinfo) {
                    return array (  '_controller' => 'ex08Bundle\\Controller\\DefaultController::linkAddressesToPersonsAction',  '_route' => 'ex08_default_linkaddressestopersons',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ex07')) {
                // ex07_default_index
                if ('/ex07' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex07Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex07_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex07_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex07_default_index'));
                    }

                    return $ret;
                }
                not_ex07_default_index:

                // ex07_default_update
                if (0 === strpos($pathinfo, '/ex07/update') && preg_match('#^/ex07/update/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ex07_default_update')), array (  '_controller' => 'ex07Bundle\\Controller\\DefaultController::updateAction',));
                }

            }

            elseif (0 === strpos($pathinfo, '/ex06')) {
                // ex06_default_index
                if ('/ex06' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex06Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex06_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex06_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex06_default_index'));
                    }

                    return $ret;
                }
                not_ex06_default_index:

                // ex06_default_update
                if (0 === strpos($pathinfo, '/ex06/update') && preg_match('#^/ex06/update/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ex06_default_update')), array (  '_controller' => 'ex06Bundle\\Controller\\DefaultController::updateAction',));
                }

            }

            elseif (0 === strpos($pathinfo, '/ex05')) {
                // ex05_default_index
                if ('/ex05' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex05Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex05_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex05_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex05_default_index'));
                    }

                    return $ret;
                }
                not_ex05_default_index:

                // ex05_default_delete
                if (0 === strpos($pathinfo, '/ex05/delete') && preg_match('#^/ex05/delete/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ex05_default_delete')), array (  '_controller' => 'ex05Bundle\\Controller\\DefaultController::deleteAction',));
                }

            }

            elseif (0 === strpos($pathinfo, '/ex04')) {
                // ex04_default_index
                if ('/ex04' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex04Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex04_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex04_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex04_default_index'));
                    }

                    return $ret;
                }
                not_ex04_default_index:

                // ex04_default_delete
                if (0 === strpos($pathinfo, '/ex04/delete') && preg_match('#^/ex04/delete/(?P<id>[^/]++)$#sD', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'ex04_default_delete')), array (  '_controller' => 'ex04Bundle\\Controller\\DefaultController::deleteAction',));
                }

            }

            elseif (0 === strpos($pathinfo, '/ex03')) {
                // ex03_default_index
                if ('/ex03' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex03Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex03_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex03_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex03_default_index'));
                    }

                    return $ret;
                }
                not_ex03_default_index:

                // ex03_default_display
                if ('/ex03/display' === $pathinfo) {
                    return array (  '_controller' => 'ex03Bundle\\Controller\\DefaultController::displayAction',  '_route' => 'ex03_default_display',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ex02')) {
                // ex02_default_index
                if ('/ex02' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex02Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex02_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex02_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex02_default_index'));
                    }

                    return $ret;
                }
                not_ex02_default_index:

                // ex02_default_display
                if ('/ex02/display' === $pathinfo) {
                    return array (  '_controller' => 'ex02Bundle\\Controller\\DefaultController::displayAction',  '_route' => 'ex02_default_display',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ex01')) {
                // ex01_default_index
                if ('/ex01' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex01Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex01_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex01_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex01_default_index'));
                    }

                    return $ret;
                }
                not_ex01_default_index:

                // ex01_default_createit
                if ('/ex01/createit' === $pathinfo) {
                    return array (  '_controller' => 'ex01Bundle\\Controller\\DefaultController::createitAction',  '_route' => 'ex01_default_createit',);
                }

            }

            elseif (0 === strpos($pathinfo, '/ex00')) {
                // ex00_default_createit
                if ('/ex00/createit' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex00Bundle\\Controller\\DefaultController::createitAction',  '_route' => 'ex00_default_createit',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex00_default_createit;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex00_default_createit'));
                    }

                    return $ret;
                }
                not_ex00_default_createit:

                // ex00_default_index
                if ('/ex00' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'ex00Bundle\\Controller\\DefaultController::indexAction',  '_route' => 'ex00_default_index',);
                    if ('/' === substr($pathinfo, -1)) {
                        // no-op
                    } elseif ('GET' !== $canonicalMethod) {
                        goto not_ex00_default_index;
                    } else {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', 'ex00_default_index'));
                    }

                    return $ret;
                }
                not_ex00_default_index:

            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if ('/' === substr($pathinfo, -1)) {
                // no-op
            } elseif ('GET' !== $canonicalMethod) {
                goto not_homepage;
            } else {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }
        not_homepage:

        if ('/' === $pathinfo && !$allow) {
            throw new Symfony\Component\Routing\Exception\NoConfigurationException();
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
