<?php

class Weather extends RestClient
{
    public function __construct()
    {
        parent::__construct(['base_url' => 'api.openweathermap.org']);
    }

    private function getWeatherStringFromCity($cityName)
    {
        $result = $this->get('data/2.5/weather', ['q' => $cityName, 'appid' => '25066d2d91c8f260965d7f7e9843b940', 'lang' => 'FR', 'units' => 'metric', 'format' => 'json']);

        if ($result->info->http_code !== 200)
            return ('Error ('.$result->info->http_code.')'.PHP_EOL);
        $arr = json_decode($result->response, true);
        return ('--- '.$arr['name'].' ---
Conditions: '.$arr['weather'][0]['description'].'
Temperature: '.$arr['main']['temp'].' degres
Humidite: '.$arr['main']['humidity'].'%'.PHP_EOL);
    }

    public function getWeatherFileFromCity($cityName)
    {
        file_put_contents('weather.txt', $this->getWeatherStringFromCity($cityName));
    }
}

?>