<?php

function get_nb($str)
{
  preg_match('/.*number:([0-9]+),.*/', $str, $match);
  return ($match[1]);
}

function str_to_elem($str)
{
  preg_match('/(\w*)\s*=\s*position:\s*([0-9]*),\s*number:([0-9]*),\s*small:\s+(\w*),\s*molar:(.*),\s* electron:([0-9\s]*)/', $str, $matches);
  return (array(
    'name' => $matches[1],
    'position' => $matches[2],
    'number' => $matches[3],
    'small' => $matches[4],
    'molar' => $matches[5],
    'electron' => $matches[6]));
}

$lst = array();

foreach (explode(PHP_EOL, file_get_contents('./ex06.txt')) as $k)
  if ($k !== '')
    $lst[get_nb($k)] = str_to_elem($k);

$file = fopen('mendeleiev.html', 'w') or die('Unable to create file mendeleiev.html!');

fwrite($file, 
'<html>

  <head>
  </head>

  <body>
    <table>
      <tr>
          <td style="border: 1px solid black; padding:10px">
          <h4>Hydrogen</h4>
          <ul>
            <li>No 42</li>
            <li>H</li>
            <li> 1.00794 </li>
          <ul>
        </td>
      </tr>
    </table>
  </body>

</html>
');

fclose($file);

?>
