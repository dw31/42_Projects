<?php

  function search_by_states($str)
  {
    $states =
    [
      'Oregon' => 'OR',
      'Alabama' => 'AL',
      'New Jersey' => 'NJ',
      'Colorado' => 'CO',
    ];

    $capitals =
    [
      'OR' => 'Salem',
      'AL' =>   'Montgomery',
      'NJ' => 'Trention',
      'KS' => 'Topeka',
    ];

    $arr = explode(', ', $str);

    foreach($arr as $k)
    {
      if (array_key_exists($k, $states) === TRUE && array_key_exists($states[$k], $capitals) === TRUE)
        printf($capitals[$states[$k]].' is the capital of '.$k.'.'.PHP_EOL);
      else if (array_search($k, $capitals) !== FALSE && array_search(array_search($k, $capitals), $states) !== FALSE)
        printf($k.' is the capital of '.array_search(array_search($k, $capitals), $states).'.'.PHP_EOL);
      else
        printf($k.' is neither a capital nor a state'.'.'.PHP_EOL);
    }
  }
?>
