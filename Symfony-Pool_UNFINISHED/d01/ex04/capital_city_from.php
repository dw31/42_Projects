<?php

  function capital_city_from($state)
  {
    $states =
    [
      'Oregon' => 'OR',
      'Alabama' => 'AL',
      'New Jersey' => 'NJ',
      'Colorado' => 'CO',
    ];

    $capitals =
    [
      'OR' => 'Salem',
      'AL' =>   'Montgomery',
      'NJ' => 'Trention',
      'KS' => 'Topeka',
    ];

    if (array_key_exists($state, $states) === FALSE || array_key_exists($states[$state], $capitals) === FALSE)
      return ('Unknown');
    return ($capitals[$states[$state]]);
  }
?>
