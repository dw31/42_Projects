<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // if game in progress, redirect to map, otherwise redirect to options
        if ($this->get('session')->get('gameInProgress') === true)
            return $this->redirectToRoute('map');
        return $this->redirectToRoute('options');
    }

    /**
     * @Route("/map/", name="map")
     */
    public function mapAction()
    {
        $hasMoved = $this->get('session')->get('hasMoved');
        $this->get('session')->set('hasMoved', false);

        if ($this->get('session')->get('gameInProgress') !== true)
            return $this->redirectToRoute('options');

        if ($this->get('session')->get('end') === false)
            return $this->render('default/map.html.twig', array('hasMoved' => $hasMoved));

        return $this->redirectToRoute('end');
    }

    /**
     * @Route("/map/{position}", name="mapPosition")
     */
    public function mapPositionAction($position)
    {
        if (in_array($position, array(11, 21, 31, 41, 51, 12, 22, 32, 42, 52, 13, 23, 33, 43, 53, 14, 24, 34, 44, 54, 15, 25, 35, 45, 55)))
        {
            $this->get('session')->set('pos', $position);
            $this->get('session')->set('hasMoved', true);
        }
        return $this->redirectToRoute('map');
    }

    /**
     * @Route("/options/", name="options")
     */
    public function optionsAction(Request $request)
    {
        return $this->render('default/options.html.twig');
    }

    /**
     * @Route("/options/new", name="optionsNew")
     */
    public function optionsNewAction(Request $request)
    {
        $form = $this->createFormBuilder()
        ->add('username', TextType::class)
        ->add('submit', SubmitType::class, array('label' => 'Create'))
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {           
            $this->get('session')->clear();
            $this->get('session')->set('username', $form->getData()['username']);
            $this->get('session')->set('gameInProgress', true);
            $this->get('session')->set('end', false);
            $this->get('session')->set('hasMoved', false);
            
            $tenFilms = array();
            foreach (array('tt7976614', 'tt7643322', 'tt5969846', 'tt4926538', 'tt4228294', 'tt1715356', 'tt1754109', 'tt0778611', 'tt0409673', 'tt0330193') as $imdbId)
            {
                $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
                $tmp = json_decode(file_get_contents('http://www.omdbapi.com/?i='.$imdbId.'&apikey=637481ee', false, $context), true);
                if (isset($tmp['Title']) && isset($tmp['Poster']))
                    $tenFilms[ $tmp['Title'] ] = $tmp['Poster'];
                $tmp = NULL;
            }
            $this->get('session')->set('moviesRemaining', $tenFilms);
            
            $this->get('session')->set('moviesOwned', 0);
            $this->get('session')->set('pos', 33);
            $this->get('session')->set('fightInProgress', false);
            $this->get('session')->set('lastSave', NULL);
            return $this->redirectToRoute('map');
        }
        return $this->render('default/optionsNew.html.twig', array('form' => $form->createView(),));
    }

    /**
     * @Route("/options/save", name="optionsSave")
     */
    public function optionsSaveAction(Request $request)
    {
        if ($this->get('session')->get('gameInProgress') === true)
        {
            $fs = new Filesystem();
            if ($fs->exists($this->get('kernel')->getRootDir().'/saves/') === false)
                $fs->mkdir($this->get('kernel')->getRootDir().'/saves/');

            $form = $this->createFormBuilder()
            ->add('submit', SubmitType::class, array('label' => 'Save'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid())
            {
                $this->get('session')->set('lastSave', time()+7200);

                $fs->dumpFile($this->get('kernel')->getRootDir().'/saves/'.$this->get('session')->get('username').'.json', json_encode(array(
                    'username'          =>      $this->get('session')->get('username'),
                    'gameInProgress'    =>      $this->get('session')->get('gameInProgress'),
                    'end'               =>      $this->get('session')->get('end'),
                    'moviesRemaining'   =>      $this->get('session')->get('moviesRemaining'),
                    'moviesOwned'       =>      $this->get('session')->get('moviesOwned'),
                    'pos'               =>      $this->get('session')->get('pos'),
                    'lastSave'          =>      $this->get('session')->get('lastSave')
                )).PHP_EOL);
            }

            if ($fs->exists($this->get('kernel')->getRootDir().'/saves/'.$this->get('session')->get('username').'.json') === true)
                $lastSave = date('d/m/Y H:i:s', $this->get('session')->get('lastSave'));
            else
                $lastSave = 'Never saved';

            return $this->render('default/optionsSave.html.twig', array('form' => $form->createView(), 'lastSave' => $lastSave));
        }
        return $this->render('default/optionsSave.html.twig');
    }

    /**
     * @Route("/options/load", name="optionsLoad")
     */
    public function optionsLoadAction(Request $request)
    {
        $finder = new Finder();
        $finder->name('*.json');
        $files = array();
        foreach ($finder->in($this->get('kernel')->getRootDir().'/saves/') as $file)
            $files[substr($file->getFilename(), 0, -5)] = substr($file->getFilename(), 0, -5);

        if (count($files) === 0)
            return $this->render('default/optionsLoad.html.twig', array('nosave' => true));
        
        $form = $this->createFormBuilder()
            ->add('username', ChoiceType::class, array('choices'  => $files))        
            ->add('submit', SubmitType::class, array('label' => 'Load'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $ret = json_decode(file_get_contents($this->get('kernel')->getRootDir().'/saves/'.$form->getData()['username'].'.json'), true);

            $this->get('session')->clear();
            $this->get('session')->set('username', $ret['username']);
            $this->get('session')->set('gameInProgress', $ret['gameInProgress']);
            $this->get('session')->set('end', $ret['end']);
            $this->get('session')->set('hasMoved', false);
            $this->get('session')->set('moviesRemaining', $ret['moviesRemaining']);
            $this->get('session')->set('moviesOwned', $ret['moviesOwned']);
            $this->get('session')->set('pos', $ret['pos']);
            $this->get('session')->set('lastSave', $ret['lastSave']);
            $this->get('session')->set('fightInProgress', false);
            return $this->redirectToRoute('map');
        }

        return $this->render('default/optionsLoad.html.twig', array('form' => $form->createView(), 'nosave' => false));
    }

    /**
     * @Route("/fight/", name="fight")
     */
    public function fightAction()
    {
        if ($this->get('session')->get('fightInProgress') === false || $this->get('session')->get('fightInProgress') === NULL)
        {
            $this->get('session')->set('fightInProgress', true);
            $this->get('session')->set('fightTurn', 1);
            $this->get('session')->set('fightPlayerHp', 100 + 100 * $this->get('session')->get('moviesOwned'));
            $this->get('session')->set('fightPlayerPower', 10 + 10 * $this->get('session')->get('moviesOwned'));
            $this->get('session')->set('fightMoviemonId', array_rand($this->get('session')->get('moviesRemaining')));
            $this->get('session')->set('fightMoviemonHp', 100 + 100 * $this->get('session')->get('moviesOwned'));
            $this->get('session')->set('fightMoviemonPower', 10 + 10 * $this->get('session')->get('moviesOwned'));
        }
        return $this->render('default/fight.html.twig');
    }

    /**
     * @Route("/fight/attack/", name="fightattack")
     */
    public function fightattackAction()
    {
        if ($this->get('session')->get('fightTurn') === 1)
        {
            $this->get('session')->set('fightTurn', 2);
            $this->get('session')->set('fightMoviemonHp', $this->get('session')->get('fightMoviemonHp') - $this->get('session')->get('fightPlayerPower'));
            if ($this->get('session')->get('fightMoviemonHp') <= 0)
            {
                $tmp = $this->get('session')->get('moviesRemaining');
                unset($tmp[$this->get('session')->get('fightMoviemonId')]);
                $this->get('session')->set('moviesRemaining', $tmp);
                $this->get('session')->set('moviesOwned', $this->get('session')->get('moviesOwned')+1);
                $this->get('session')->set('fightInProgress', false);
                $this->get('session')->remove('fightTurn');
                $this->get('session')->remove('fightPlayerHp');
                $this->get('session')->remove('fightPlayerPower');
                $this->get('session')->remove('fightMoviemonId');
                $this->get('session')->remove('fightMoviemonHp');
                $this->get('session')->remove('fightMoviemonPower');
                if ($this->get('session')->get('moviesOwned') < 10)
                    return $this->redirectToRoute('map');
                $this->get('session')->set('end', 'win');
                return $this->redirectToRoute('end');
            }
        }
        return $this->redirectToRoute('fight');
    }

    /**
     * @Route("/fight/nextturn/", name="fightnextturn")
     */
    public function fightnextturnAction()
    {
        if ($this->get('session')->get('fightTurn') === 2)
        {
            $this->get('session')->set('fightTurn', 1);
            $this->get('session')->set('fightPlayerHp', $this->get('session')->get('fightPlayerHp') - $this->get('session')->get('fightMoviemonPower'));
            if ($this->get('session')->get('fightPlayerHp') <= 0)
            {
                $this->get('session')->set('end', 'lose');
                $this->get('session')->set('fightInProgress', false);
                $this->get('session')->remove('fightTurn');
                $this->get('session')->remove('fightPlayerHp');
                $this->get('session')->remove('fightPlayerPower');
                $this->get('session')->remove('fightMoviemonId');
                $this->get('session')->remove('fightMoviemonHp');
                $this->get('session')->remove('fightMoviemonPower');
                return $this->redirectToRoute('end');
            }
        }
        else
            $this->get('session')->set('fightTurn', 2);
        return $this->redirectToRoute('fight');
    }

    /**
     * @Route("/fight/runaway/", name="fightrunaway")
     */
    public function fightrunawayAction()
    {
        $this->get('session')->set('fightInProgress', false);
        $this->get('session')->remove('fightTurn');
        $this->get('session')->remove('fightPlayerHp');
        $this->get('session')->remove('fightPlayerPower');
        $this->get('session')->remove('fightMoviemonId');
        $this->get('session')->remove('fightMoviemonHp');
        $this->get('session')->remove('fightMoviemonPower');
        return $this->redirectToRoute('map');
    }

    /**
     * @Route("/end/", name="end")
     */
    public function endAction()
    {
        return $this->render('default/end.html.twig');
    }
}
