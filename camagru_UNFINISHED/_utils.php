<?php 

  function utils_exitWithFooter()
  {
    include('footer.php');
    exit();
  }

  function utils_displayImgWithDetails()
  {
    if (isset($_GET['display']) && $_GET['display'] === '1'
    && isset($_GET['from']) && ($_GET['from'] === 'g' || $_GET['from'] === 'w')
    && isset($_GET['id']) && is_numeric($_GET['id'])
    && isset($_GET['page']) && is_numeric($_GET['page'])
    && db_isImgExists($_GET['id']))
    {
      echo '<img src="tmp/'.$_GET['id'].'.jpg" style="display:block; margin: 0 auto;"></img>';
      utils_exitWithFooter();
    }
  }

?>
