<?php
  require('header.php');

  utils_displayImgWithDetails();

  // display existings
    foreach (db_getCurrentPageGallery() as $k)
      echo '<a href="?page='.(db_currentPageNum()).'&display=1&from=g&id='.$k.'">'.$k.'.jpg</a><br/>';
  //

  // previous page link
    if (db_currentPageNum() > 1)
      echo '<a href="?page='.(db_currentPageNum()-1).'">Previous</a> ... ';
  //

  // current page nb
    if (db_currentPageNum())
      echo 'Page '.db_currentPageNum();
  //

  // next page link
    if (db_currentPageNum() < db_getNbOfPageGallery())
      echo ' ... <a href="?page='.(db_currentPageNum()+1).'">Next</a>';
  //

  require('footer.php');
?>
