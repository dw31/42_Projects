<?php 
error_reporting(E_ALL);
ini_set('display_errors', 1);
  // start session
    session_start();
  //
  
  // set local to FR
    setlocale (LC_TIME, 'fr_FR.utf8','fra');
  //

  // db functions
    require_once('_db.php');
    require_once('_user.php');
    require_once('_utils.php');
  //

  // handle connection (cannot be done later)
    $ret_st = 0;
    if (isset($_POST['submit_type']) && $_POST['submit_type'] == 'auth_userLogin')
      $ret_st = db_connectUser($_POST['auth_userLogin_name'], hash('whirlpool', $_POST['auth_userLogin_passwd']));
  //

  // head
    echo '
      <html>
          <head>
            <link rel="stylesheet" type="text/css" href="style.css">
            <title>Camagru</title>
          </head>';
  //

  // body
    echo '
      <body>
    ';
  //

  // menu bar
    echo
    '
      <div class="header_barcontainer">
        <div class="header_barcontainer_left">C<i><b>A</b></i>M<i><b>A</b></i>GRU</div>
        <div class="header_barcontainer_right">
          <form action="index.php"><input type="submit" value="Home"/></form>
          <form action="gallery.php"><input type="submit" value="Gallery"/></form>
          <form action="workshop.php"><input type="submit" value="Workshop"/></form>
            '. (($_SESSION['user_isConnected'] === true)
            ? '<form method="GET"><input type=hidden name=logout value=1 /><input type="submit" value="Disconnect"/></form>'
            : '<form action="connect.php"><input type="submit" value="Connect"/></form>').'
        </div>
      </div>
      <br/>
    ';
  //

?>
