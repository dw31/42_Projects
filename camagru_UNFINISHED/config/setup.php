<?php

  include "database.php";

  try
  {
    $db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

    $db->exec('
      DROP DATABASE IF EXISTS `camagru`;
      CREATE DATABASE `camagru` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
    ');

    $db->exec('
      USE `camagru`;

      DROP TABLE IF EXISTS `camagru`.`cmg_user`;
      CREATE TABLE `camagru`.`cmg_user` (`id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY, `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `name` VARCHAR(50) NOT NULL, `passwd` TEXT NOT NULL, `mail` TEXT NOT NULL, `isAdmin` TINYINT(1) DEFAULT 0 NOT NULL, `isActivated` TINYINT(1) DEFAULT 0 NOT NULL);
      INSERT INTO `camagru`.`cmg_user` (`name`, `passwd`, `mail`, `isAdmin`, `isActivated`) VALUES ("admin", "1fae2c654fa4fd7e6f1b017d0cc33dba66b3a1ff4954628cab9d17af43f8379b280e946ed81602ba01622f7f38387cb11764a1efb9c9682e5962ff1e62c1e8e4", "wide-aze@student.42.fr", 1, 1);

      DROP TABLE IF EXISTS `camagru`.`cmg_img`;
      CREATE TABLE `camagru`.`cmg_img` (`id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY, `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `userId` BIGINT(20) NOT NULL);

      DROP TABLE IF EXISTS `camagru`.`cmg_comment`;
      CREATE TABLE `camagru`.`cmg_comment` (`id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY, `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `userId` BIGINT(20) NOT NULL, `imgId` BIGINT(20) NOT NULL, `comment` TEXT NOT NULL);

      DROP TABLE IF EXISTS `camagru`.`cmg_like`;
      CREATE TABLE `camagru`.`cmg_like` (`id` BIGINT(20) AUTO_INCREMENT PRIMARY KEY, `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, `userId` BIGINT(20) NOT NULL, `imgId` BIGINT(20) NOT NULL);
    ');
    printf('Database ready !'.PHP_EOL);
  }
  catch (PDOException $e)
  {
    printf('Error: %s'.PHP_EOL, $e->getMessage());
  }
  
?>
