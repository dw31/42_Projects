<?php

  // PDO connection
    include_once('config/database.php');
    try
    {
      $db = new PDO($DB_DSN, $DB_USER, $DB_PASSWORD);
      $db->exec(' USE camagru; ');
    }
    catch (PDOException $e)
    {
      printf('FATAL ERROR: %s'.PHP_EOL, $e->getMessage());
      include('footer.php');
      exit();
    }
  //

  function db_isUserTknValid($tkn)
  {
    $arr = explode('/', $tkn, 2);
    if ($arr === false || count($arr) !== 2)
      return (false);
    $query = $GLOBALS['db']->prepare(" SELECT id FROM cmg_user WHERE name = ? AND passwd = ? ");
    $query->execute($arr);
    if ($query->fetch(PDO::FETCH_NUM) !== false)
      return (true);
    return (false);
  }

  function db_isUserExists($name)
  {
    $query = $GLOBALS['db']->prepare(" SELECT * FROM cmg_user WHERE name = ? LIMIT 1 ");
    $query->bindParam(1, $name, PDO::PARAM_STR);
    $query->execute();
    if ($query->fetch(PDO::FETCH_ASSOC) === false)
      return (false);
    return (true);
  }

  function db_connectUser($name, $passwd)
  {
    $query = $GLOBALS['db']->prepare(" SELECT * FROM cmg_user WHERE name = ? AND passwd = ? ");
    $query->bindParam(1, $name, PDO::PARAM_STR);
    $query->bindParam(2, $passwd, PDO::PARAM_STR);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    if ($row === false)
      return (1);
    if (!$row['isActivated'])
      return (2);
    $_SESSION['user_tkn'] = $name.'/'.$passwd;
    $_SESSION['user_isConnected'] = true;
    $_SESSION['user_id'] = $row['id'];
    $_SESSION['user_username'] = $row['name'];
    $_SESSION['user_mail'] = $row['mail'];
    $_SESSION['user_isAdmin'] = $row['isAdmin'];
    return (0);
  }
  
  function db_signupUser($name, $passwd, $mail)
  {
    // check if user already exists
      $query = $GLOBALS['db']->prepare(" SELECT * FROM cmg_user WHERE LOWER(name) = LOWER(?) LIMIT 1 ");
      $query->bindParam(1, $name, PDO::PARAM_STR);
      $query->execute();
      if ($query->fetch(PDO::FETCH_ASSOC) !== false)
        return (1);
    //

    // check if mail already exists
      $query = $GLOBALS['db']->prepare(" SELECT * FROM cmg_user WHERE LOWER(mail) = LOWER(?) LIMIT 1 ");
      $query->bindParam(1, $mail, PDO::PARAM_STR);
      $query->execute();
      if ($query->fetch(PDO::FETCH_ASSOC) !== false)
        return (2);
    //

    // insert in db
      $query = $GLOBALS['db']->prepare(" INSERT INTO cmg_user (`name`, `passwd`, `mail`) VALUES (?, ?, ?) ");
      $query->bindParam(1, $name, PDO::PARAM_STR);
      $query->bindParam(2, $passwd, PDO::PARAM_STR);
      $query->bindParam(3, $mail, PDO::PARAM_STR);
      $query->execute();
    //

    // send confirmation mail 
    //

    return (0);
  }

  function db_currentPageNum()
  {
    if (isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] > 0)
      return ($_GET['page']);
    else 
      return (1);
  }

  function db_getNbOfPageGallery()
  {
    $query = $GLOBALS['db']->prepare(" SELECT count(*) AS 'nb' FROM cmg_img ");
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return ($row['nb']/9);
  }

  function db_getNbOfPageWorkshop()
  {
    $query = $GLOBALS['db']->prepare(" SELECT count(*) AS 'nb' FROM cmg_img WHERE userId = ? ");
    $query->bindParam(1, $_SESSION['user_id'], PDO::PARAM_INT);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return ($row['nb']/9);
  }

  function db_getCurrentPageGallery()
  {
    $query = $GLOBALS['db']->prepare(" SELECT id FROM cmg_img ORDER BY id DESC LIMIT ".((db_currentPageNum()-1)*9).",9 ");
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    $arr = array();
    while ($row !== false)
    {
      $arr[] = $row['id'];
      $row = $query->fetch(PDO::FETCH_ASSOC);
    }
    return ($arr);
  }

  function db_getCurrentPageWorkshop()
  {
    $query = $GLOBALS['db']->prepare(" SELECT id FROM cmg_img WHERE userId = ? ORDER BY id DESC LIMIT ".((db_currentPageNum()-1)*9).',9 ');
    $query->bindParam(1, $_SESSION['user_id'], PDO::PARAM_INT);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    $arr = array();
    while ($row !== false)
    {
      $arr[] = $row['id'];
      $row = $query->fetch(PDO::FETCH_ASSOC);
    }
    return ($arr);
  }

  function db_isImgExists($id)
  {
    $query = $GLOBALS['db']->prepare(" SELECT count(*) AS 'bool' FROM cmg_img WHERE id = ? ");
    $query->bindParam(1, $id, PDO::PARAM_INT);
    $query->execute();
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return ($row['bool']);
  }

?>
