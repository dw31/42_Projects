<?php

  require('header.php');

  if (isset($_POST['submit_type']))
  {
    // connectUser
      if ($_POST['submit_type'] === 'auth_userLogin')
      {
        /* pre-process done in header */
        if ($ret_st == 1)
          echo '<div class="misc_errMsg">Invalid user/password</div>';
        else if ($ret_st == 2)
          echo 'DBG_HERE_SEND_MAIL<div class="misc_errMsg">Your account is not activated. Please click on the link in the confirmation mail</div>';
      }
    //

    // signupUser
      else if ($_POST['submit_type'] === 'auth_userSignup')
      {
        if (!user_isUserValid($_POST['auth_userSignup_name']))
          echo '<div class="misc_errMsg">Invalid username (alphanumerics/underscore only)</div>';
        else if (!user_isPasswordSecure($_POST['auth_userSignup_passwd']))
          echo '<div class="misc_errMsg">Password must have at least 8 characters: one upper, one lower, one number, one special character</div>';
        else if (!user_isMailValid($_POST['auth_userSignup_mail']))
          echo '<div class="misc_errMsg">Invalid mail</div>';
        else
        {
          $ret_st = db_signupUser($_POST['auth_userSignup_name'], hash('whirlpool', $_POST['auth_userSignup_passwd']), $_POST['auth_userSignup_mail']);
          if (!$ret_st)
            echo 'DBG_HERE_SEND_MAIL<div class="misc_centertextblue">Your account has been created. Click on the confirmation link sent by mail to activate it.</div>';
          else if ($ret_st == 1)
            echo '<div class="misc_errMsg">Username already used</div>';
          else if ($ret_st == 2)
            echo '<div class="misc_errMsg">An account already exists with this mail.</div>';
        }
      }
    //

    // reset password
      else if ($_POST['submit_type'] === 'auth_userReset')
      {
        if (db_isUserExists($_POST['auth_userReset_name']))
          echo 'DBG_HERE_SEND_MAIL<div class="misc_centertextblue">Click on the link sent by mail to reset your password.</div>';
        else
          echo '<div class="misc_errMsg">Invalid username</div>';
      }
    //
  }

  // action if user is connected or not
    if ($_SESSION['user_isConnected'] === true)
    {
      echo'
        <div class="misc_centertextblue">
          Hello '.$_SESSION['user_username'].' !
          <br/>
        </div>
      ';
    }
    else
    {
      echo(user_getLoginForm());
      echo '</br>';
      echo(user_getSignupForm());
      echo '</br>';
      echo(user_getResetForm());
    }
  //

  require('footer.php');
?>
