<?php

  function user_clearSession()
  {
    $_SESSION = NULL;
    $_SESSION['user_tkn'] = NULL;
    $_SESSION['user_isConnected'] = false;
    $_SESSION['user_username'] = NULL;
    $_SESSION['user_mail'] = NULL;
    $_SESSION['user_isAdmin'] = 0;
  }

  function user_checkUserTkn()
  {
    if (empty($_SESSION['user_tkn']) || !db_isUserTknValid($_SESSION['user_tkn']))
      user_clearSession();
  }

  function user_logout() { user_clearSession(); }

  function user_getLoginForm()
  {
    return ('
      <form method="POST">
        <input type=hidden name=submit_type value=auth_userLogin /><br/>
        <input type=text name=auth_userLogin_name placeholder="user" required />
        <input type=password name=auth_userLogin_passwd placeholder="password" required />
        <input type=submit name=Submit value="Login" />
      </form>
    ');
  }
  
  function user_getSignupForm()
  {
    return ('
      <form method="POST">
        <input type=hidden name=submit_type value=auth_userSignup /><br/>
        <input type=text name=auth_userSignup_name placeholder="user" required />
        <input type=password name=auth_userSignup_passwd placeholder="password" required />
        <input type=text name=auth_userSignup_mail placeholder="mail" required />
        <input type=submit name=Submit value="Signup" />
      </form>
    ');
  }
  
  function user_getResetForm()
  {
    return ('
      <form method="POST">
        <input type=hidden name=submit_type value=auth_userReset /><br/>
        <input type=text name=auth_userReset_name placeholder="user" required />
        <input type=submit name=Submit value="Reset password" />
      </form>
    ');
  }

  function user_needAConnectedUser()
  {
    if ($_SESSION['user_isConnected'] !== true)
    {
      echo '<div class="misc_errMsg">A connected user is required to access this page</div>';
      utils_exitWithFooter();
    }
  }

  function user_isUserValid($name)
  {
    if (!preg_match('/^[\w|_]+$/', $name))
      return (false); 
    return (true); 
  }
  
  function user_isPasswordSecure($passwd)
  {
    if (strlen($passwd) < 8
    || !preg_match('@[A-Z]@', $passwd)
    || !preg_match('@[a-z]@', $passwd)
    || !preg_match('@[0-9]@', $passwd)
    || !preg_match('@\`\~\!\@\#\$\%\^\&\*\(\)\_\+\=\[\]\{\}\;\:\'\"\,\.\/\<\>\?\\\|@', $passwd))
      return (false); 
    return (true);
  }
  
  function user_isMailValid($mail)
  {
    if (!preg_match('/^.+@.+\.\w/', $mail))
      return (false); 
    return (true);
  }

//////// done everytime

  if (isset($_GET) && isset($_GET['logout']) && $_GET['logout'] === '1')
    user_clearSession();
  
  user_checkUserTkn();

///////

?>
